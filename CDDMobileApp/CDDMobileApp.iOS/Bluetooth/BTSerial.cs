﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExternalAccessory;
using Foundation;
using UIKit;
using CDDMobile.Network;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(CDDMobile.iOS.Bluetooth.BTSerial))]
namespace CDDMobile.iOS.Bluetooth
{
    public class BTSerial : IBTSerial
    {

        private EAAccessoryManager mgr = EAAccessoryManager.SharedAccessoryManager;
        private EAAccessory[] accessories;

        public BTSerial()
        {
            accessories = mgr.ConnectedAccessories;
        }

        public string DeviceName { get; set; }

        public bool HasAdapter => throw new NotImplementedException();

        public bool AdapterEnabled => throw new NotImplementedException();

        public bool EnableAdapter()
        {
            throw new NotImplementedException();
        }

        public IList<BTDevice> GetDeviceList(int btclass = 1574432)
        {
            try
            {
                var devOut = new List<BTDevice>();

                foreach (var acc in accessories)
                {
                    Console.WriteLine(string.Format("Accessory found: {0} with class {1}", acc.Name, acc.Class.ToString()));



                    devOut.Add(new BTDevice(acc.ConnectionID.ToString(), acc.Name, acc.Class.ToString()));
                }

                return devOut;
            }
            catch
            {
                return null;
            }

        }

        public void OpenBluetoothSettings()
        {
            throw new NotImplementedException();
        }

        Task<ISocketStream> IBTSerial.GetSocket()
        {
            throw new NotImplementedException();
        }


    }
}