﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CDDMobile.Interfaces;
using Xamarin.Forms;
using Firebase.Auth;
using System.Threading.Tasks;

[assembly: Dependency(typeof(CDDMobile.iOS.FirebaseAuth))]
namespace CDDMobile.iOS
{
    public class FirebaseAuth : IAuth
    {

        public async Task<string> LoginWithEmailPassword(string email, string password)
        {
            try
            {
                var user = await Auth.DefaultInstance.SignInWithPasswordAsync(email, password);
                return await user.User.GetIdTokenAsync();
            }
            catch (Exception e)
            {
                return "";
            }

        }



    }

}