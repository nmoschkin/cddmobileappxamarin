﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreLocation;
using Foundation;
using SystemConfiguration;
using UIKit;

namespace CDDMobile.iOS
{
    public class SSIDHelper : ISSIDHelper
    {

        bool? b = null;

        private bool GetLocationConsent()
        {
            if (b == true) return true;

            var manager = new CLLocationManager();

            manager.AuthorizationChanged += (sender, args) => 
            {
                b = (args.Status == CLAuthorizationStatus.Authorized || args.Status == CLAuthorizationStatus.AuthorizedWhenInUse);
            };

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                manager.RequestWhenInUseAuthorization();

            DateTime timeOut = DateTime.Now.AddSeconds(10);

            do
            {
                Task.Yield();
            } while (b == null || DateTime.Now > timeOut);

            return ((bool)b);
        }

        public string GetSSID()
        {
            if (!GetLocationConsent()) return null;

            string ssid = "";
            try
            {
                string[] supportedInterfaces;
                StatusCode status;
                if ((status = CaptiveNetwork.TryGetSupportedInterfaces(out supportedInterfaces)) != StatusCode.OK)
                {

                }
                else
                {
                    foreach (var item in supportedInterfaces)
                    {
                        NSDictionary info;
                        status = CaptiveNetwork.TryCopyCurrentNetworkInfo(item, out info);
                        if (status != StatusCode.OK)
                        {
                            continue;
                        }
                        ssid = info[CaptiveNetwork.NetworkInfoKeySSID].ToString();
                    }
                }
            }
            catch
            {

            }

            return ssid;
        }

    }
}