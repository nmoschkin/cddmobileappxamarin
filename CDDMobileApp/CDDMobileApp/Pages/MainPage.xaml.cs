﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CDDMobile.ViewModels;
using CDDMobile.Views;
using Acr.UserDialogs;

namespace CDDMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage() : this(null)
        {

        }

        public MainPage(View content)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            var mp = ((App)Application.Current).MainPage;

            if (content != null)
            {
                NavBar.IsBackArrowVisible = true;
            }
            else
            {
                NavBar.IsBackArrowVisible = false;
            }

            if (content == null)
            {
                content = new MainView();
            }

            ContentArea.Children.Add(content);
            BindingContext = content.BindingContext;

        }
        protected override void OnSizeAllocated(double width, double height)
        {

            ContentArea.HeightRequest = height - TopGrid.Height;
            
            base.OnSizeAllocated(width, height);


        }
        private async void BackButton_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void NavBar_MenuClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.MainPage(new BluetoothView()));
        }
    }
}