﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CDDMobile
{
    public static class Settings
    {
        // Language Settings
        private const string DeviceNameKey = "DeviceName";
        private const string ShowVisibleKey = "ShowVisible";
        private const string ShowUVKey = "ShowUV";
        private const string ShowIRKey = "ShowIR";
        private const string ShowImprintKey = "ShowImprint";

        private const string UseWiFiKey = "UseWifi";

        public static string DeviceName
        {

            get => Preferences.Get(DeviceNameKey, null);
            set
            {
                Preferences.Remove(DeviceNameKey);
                Preferences.Set(DeviceNameKey, value);
            }

        }

        public static bool UseWiFi
        {
            get => Preferences.Get(UseWiFiKey, true);
            set
            {
                Preferences.Remove(UseWiFiKey);
                Preferences.Set(UseWiFiKey, value);
            }

        }


        public static bool ShowVisible
        {
            get => Preferences.Get(ShowVisibleKey, true);
            set
            {
                Preferences.Remove(ShowVisibleKey);
                Preferences.Set(ShowVisibleKey, value);
            }
            
        }

        public static bool ShowUV
        {
            get => Preferences.Get(ShowUVKey, true);
            set
            {
                Preferences.Remove(ShowUVKey);
                Preferences.Set(ShowUVKey, value);
            }

        }

        public static bool ShowIR
        {
            get => Preferences.Get(ShowIRKey, true);
            set
            {
                Preferences.Remove(ShowIRKey);
                Preferences.Set(ShowIRKey, value);
            }

        }


        public static bool ShowImprint
        {
            get => Preferences.Get(ShowImprintKey, true);
            set
            {
                Preferences.Remove(ShowImprintKey);
                Preferences.Set(ShowImprintKey, value);
            }

        }

        public static string GetSSID()
        {
            ISSIDHelper helper = DependencyService.Get<ISSIDHelper>();
            return helper?.GetSSID();
        }


    }
}
