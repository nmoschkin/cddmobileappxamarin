﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile
{
    public interface ISSIDHelper
    {
        string GetSSID();
    }
}
