﻿using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace CDDMobile
{
    public static class Constants
    {

        // MS App Center
        public const string AndroidAppCenterKey = "27efc9b0-ae52-4281-b655-f2942830c0ec";

        public const string iOSAppCenterKey = "ca92525e-71f3-4d29-a5aa-d9e407253862";
        //


        // Firebase Database
        public const string ApiKey = "AIzaSyBV1U3xzfFux8FXSsrSO495qXDjiRQRjHU";
        public const string TFApiKey = "AIzaSyCBa7yuYV54YZ4cLR71ep2wdAtYQz1DEMk";

        public const string TFBucketPath = "cdd_2020/models/model-export/icn";

        public const string DatabaseURL = "https://cdd-webapp-test.firebaseio.com";

        public const string StorageBucket = "cdd-webapp-test.appspot.com";
        public const string TFStorageBucket = "counterfeit-detection-vcm.appspot.com";

        public const string ProcessTriggerURL = "http://ec2-18-221-191-149.us-east-2.compute.amazonaws.com:5000/trigger/{0}";
        
        public const string AugmentationURL = "http://ec2-3-17-157-160.us-east-2.compute.amazonaws.com:5000/data_augmentation/{0}";

        public const string TriggerAutoMLTrainingURL = "http://ec2-3-17-157-160.us-east-2.compute.amazonaws.com:5000/automl_training/{0}";


        public const string AutoMLTrainingBucket = "autotraining-automl";

        public const string Username = "precise.tester@gmail.com";
        public const string Password = "Precise@123";
        //


    }


}
