﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

using CDDMobile.ViewModels;
using System.Reflection;
using CDDMobile.Database;
using CDDMobile.Localization.Resources;

namespace CDDMobile.Converters
{
    public class EnumToTextConverter : IValueConverter
    {

        public Type EnumType { get; set; }


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TrainingType t)
            {
                switch (t)
                {
                    case TrainingType.Authentic:
                        return AppResources.Authentic;

                    case TrainingType.Counterfeit:
                        return AppResources.Counterfeit;

                    default:
                        return "";
                }
            }
            else if (value is Enum e)
            {
                var fields = e.GetType().GetFields(BindingFlags.Public | BindingFlags.Static);

                foreach (var field in fields)
                {
                    if ((int)field.GetValue(null) == (int)value)
                    {

                        var att = field.GetCustomAttribute<ResourceReferenceAttribute>();

                        if (att != null)
                        {
                            return att.Translate();
                        }
                        else
                        {
                            return e.ToString();
                        }
                    }
                }

            }

            return value?.ToString();
        
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType == typeof(TrainingType) && string.IsNullOrEmpty((string)value)) 
            {
                return TrainingType.None;
            }
            else if (targetType.BaseType == typeof(Enum))
            {
                var fields = targetType.GetFields(BindingFlags.Public | BindingFlags.Static);

                foreach (var field in fields)
                {
                    var att = field.GetCustomAttribute<ResourceReferenceAttribute>();
                    if (att != null)
                    {
                        if (att.Translate() == (string)value)
                        {
                            return field.GetValue(null);   
                        }
                    }
                    else if (field.Name == (string)value)
                    {
                        return field.GetValue(null);
                    }
                }

                return value;

            }
            else
            {
                return value;
            }
        }
    }

}
