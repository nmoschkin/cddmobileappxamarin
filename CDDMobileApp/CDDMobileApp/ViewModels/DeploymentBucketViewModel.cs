﻿using CDDMobile.Database.Models;
using CDDMobile.Localization.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using CDDMobile.Database;
using Xamarin.Forms;
using System.IO;
using CDDMobile.Network;
using System.Threading;
using Telerik.XamarinForms.Chart;

namespace CDDMobile.ViewModels
{

    
    public class DeploymentBucketViewModel : ViewModelBase
    {

        public delegate void BucketClickedEvent(object sender, EventArgs e);
        public event BucketClickedEvent BucketClicked;

        public delegate void TransmitCompleteEvent(object sender, ProgressEventArgs e);
        public event TransmitCompleteEvent TransmitComplete;

        private ObservableCollection<DeploymentBucket> buckets = new ObservableCollection<DeploymentBucket>();
        private float progress = 0;
        public float Progress
        {
            get => progress;
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<DeploymentBucket> Buckets
        {
            get => buckets;
            internal set
            {
                buckets = value;
                OnPropertyChanged();
            }
        }

        public DeploymentBucketViewModel()
        {
            Title = AppResources.DeploymentStatus;


        }

        public async Task RefreshBuckets()
        {

            //Acr.UserDialogs.Forms.ToastConfig cfg = new Acr.UserDialogs.Forms.ToastConfig()
            //{
            //    Position = Acr.UserDialogs.Forms.ToastPosition.Bottom,
            //    Message = AppResources.RetrievingData,
            //    DisplayTime = new TimeSpan(0, 0, 1)
            //};

            //var dlg = new Acr.UserDialogs.Forms.UserDialogs();
            //dlg.Toast(cfg);

            //if (Buckets.Count > 0)
            //{
            //    foreach (var b in Buckets) b.Clicked -= Deployment_Clicked;
            //}

            Buckets.Clear();

            var db = new DrugDBHelper();

            var dict = await db.GetDrugData();

            var bucket = await db.GetCFDDBucket();

            var models = await db.GetTrainedModels(bucket);

            foreach (var model in models)
            {
                var deployment = await db.GetDeploymentBucket(model, bucket);

                foreach (var entry in dict)
                {
                    if (deployment.DrugId == entry.Value.DrugId)
                    {
                        deployment.Drug = new Database.ViewModels.DrugViewModel(entry.Value);
                        break;
                    }
                }

                if (deployment.Drug != null)
                {
                    deployment.Clicked += Deployment_Clicked;
                    Buckets.Add(deployment);
                }

            }

        }


        public void BeginTransmitTrainingModel(CancellationToken cancel, DeploymentBucket bucket = null)
        {
            if (bucket == null)
            {
                if (SelectedItem == null) return;
                bucket = (DeploymentBucket)SelectedItem;
            }

            var dbh = new DrugDBHelper();
            string oldStatus = Status;

            dbh.Progress += (sender, e) =>
            {
                Device.BeginInvokeOnMainThread(() => Progress = ((float)e.Position / (float)e.Total));
            };

            var trThread = new Task(async () =>
            {
                try
                {
                    var bth = new BluetoothConnection();
                    string oldStatus = null;

                    bth.NotifyProgress += (sender, e) =>
                    {
                        Device.BeginInvokeOnMainThread(() => {
                            Progress = ((float)e.Position / (float)e.Total);
                            if (!string.IsNullOrEmpty(e.Message))
                            {
                                oldStatus = Status;
                                Status = e.Message;
                            }
                            else if (oldStatus != null)
                            {
                                Status = oldStatus;
                                oldStatus = null;
                            }
                        });
                    };

                    bucket.FileStreams = new Stream[bucket.Files.Length];

                    bool b;
                    int i = 0;

                    foreach (var f in bucket.Files)
                    {
                        await Device.InvokeOnMainThreadAsync(() =>
                            Status = string.Format(AppResources.DownloadingX, Path.GetFileName(f))
                        );

                        bucket.FileStreams[i] = await dbh.DownloadBucketItem(f);

                        if (cancel.IsCancellationRequested)
                        {
                            Device.BeginInvokeOnMainThread(() => { Status = AppResources.TaskCanceled; Progress = 0; });
                            TransmitComplete?.Invoke(this, new ProgressEventArgs(ConnectionState.Cancel));
                            return;
                        }
                        else if (bucket.FileStreams[i] == null)
                        {
                            Device.BeginInvokeOnMainThread(() => { Status = AppResources.TaskFailed; Progress = 0; });
                            TransmitComplete?.Invoke(this, new ProgressEventArgs(ConnectionState.Error));
                            return;
                        }

                        var it = FileToImgType(f);

                        await Device.InvokeOnMainThreadAsync(() =>
                            Status = string.Format(AppResources.TransmittingXToDevice, it.ToString())
                        );

                        b = await bth.TransmitModel(bucket.DrugId, it, bucket.FileStreams[i], cancel);

                        if (cancel.IsCancellationRequested)
                        {
                            Device.BeginInvokeOnMainThread(() => { Status = AppResources.TaskCanceled; Progress = 0; });
                            TransmitComplete?.Invoke(this, new ProgressEventArgs(ConnectionState.Cancel));
                            return;
                        }
                        else if (!b)
                        {

                            Device.BeginInvokeOnMainThread(() => { Status = AppResources.TaskFailed; Progress = 0; });
                            TransmitComplete?.Invoke(this, new ProgressEventArgs(ConnectionState.Error));
                            return;
                        }

                        i++;
                    }

                    await Device.InvokeOnMainThreadAsync(() =>
                    {
                        TransmitComplete?.Invoke(this, new ProgressEventArgs(ConnectionState.Done));
                        Progress = 0;
                        Status = AppResources.Done;

                    });
                }
                catch
                {
                    return;
                }

                return;

            }, cancel);

            trThread.Start();

        }

        private ImageType FileToImgType(string fn)
        {

            if (fn.IndexOf("/tflite-uv_") != -1)
                return ImageType.Ultraviolet;

            else  if (fn.IndexOf("/tflite-ir_") != -1)
                return ImageType.Infrared;

            else if (fn.IndexOf("/tflite-imp_") != -1)
                return ImageType.Imprint;

            return ImageType.Visible;
        }

        private void Deployment_Clicked(object sender, EventArgs e)
        {
            SelectedItem = sender;
            BucketClicked?.Invoke(sender, e);
        }

    }
}
