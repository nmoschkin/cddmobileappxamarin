﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Telerik.XamarinForms.Input;

using CDDMobile.Filters;
using CDDMobile.Localization.Resources;
using CDDMobile.Database;
using CDDMobile.Database.ViewModels;
using Telerik.XamarinForms.Input.AutoComplete;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace CDDMobile.ViewModels
{
    public class DrugSearchViewModel : ViewModelBase
    {

        public DrugSearchViewModel()
        {
            
            Device.BeginInvokeOnMainThread(async () =>
            {
                DrugFilter = new DrugSearchFilter();

                var db = new DrugDBHelper();
#if DEBUG
                Debugger.NotifyOfCrossThreadDependency();
#endif
                if (await db.SignIn())
                {
                    var d = await db.GetDrugViewModels();
                    Drugs = d;
                }
                else
                {
                    Drugs = new ObservableCollection<DrugViewModel>();
                }

            });

        }


        private ObservableCollection<DrugViewModel> drugs;

        public ObservableCollection<DrugViewModel> Drugs
        {
            get => drugs;
            set
            {
                if (drugs == value) return;
                drugs = value;

                OnPropertyChanged();
            }
        }

        private DrugSearchFilter drugSearchFilter;

        public DrugSearchFilter DrugFilter
        {
            get => drugSearchFilter;
            set
            {
                if (drugSearchFilter == value) return;

                drugSearchFilter = value;
                OnPropertyChanged();
            }
        }

        //private object selectedDrug;

        //public object SelectedDrug
        //{
        //    get => selectedDrug;
        //    set
        //    {
        //        if (selectedDrug == value) return;
        //        selectedDrug = value;
        //        OnPropertyChanged();
        //    }
        //}

        private TrainingType trainingType;

        public TrainingType TrainingType
        {
            get => trainingType;
            set
            {
                if (trainingType == value) return;
                trainingType = value;
                OnPropertyChanged();
            }
        }

    }
}
