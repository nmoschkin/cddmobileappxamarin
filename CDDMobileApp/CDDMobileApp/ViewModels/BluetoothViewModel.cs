﻿using CDDMobile.Localization.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;
using CDDMobile.Network;
using CDDMobile.ViewModels;
using System.Collections.ObjectModel;
using CDDMobile.Database.ViewModels;
using System.Threading.Tasks;
using CDDMobile.Views;
using CDDMobile.Database;
using CDDMobile.Database.Models;
using System.Net.Http;

namespace CDDMobile.ViewModels
{
    public class BluetoothViewModel : ViewModelBase
    {

        private BluetoothConnection conn;

        private BTDevice selDev;

        private TrainingImageViewModel trainingInfo;

        private DrugViewModel drug; 

        private ObservableCollection<BTDevice> devices = new ObservableCollection<BTDevice>();

        private float progress = 0;

        public BluetoothViewModel()
        {
            conn = new BluetoothConnection();
            RefreshDeviceList();
        }

        public BluetoothViewModel(DrugViewModel drug) : this()
        {
            Drug = drug;
        }

        public void RefreshDeviceList()
        {
            if (BluetoothConnection.CheckAdapterState())
            {
                var tdev = BluetoothConnection.GetPairedDeviceList();
                Devices.Clear();

                foreach(var td in tdev)
                {
                    Devices.Add(td);
                }

                if (!string.IsNullOrEmpty(Settings.DeviceName))
                {
                    foreach (var dev in Devices)
                    {
                        if (dev.Name == Settings.DeviceName)
                        {
                            SelectedDevice = dev;
                            break;
                        }
                    }
                }
            }
        }

        public async Task<CommandResult?> Capture(bool detect = false, string drugId = null)
        {
            var res = await conn.Capture(detect, drugId);
            if (res == null) return null;

            TrainingInfo = new TrainingImageViewModel(res);
            return res.Result;
        }

        public async Task<bool> UploadTrainingImages()
        {
            var dbh = new DrugDBHelper();
            var fileName = Guid.NewGuid().ToString("d") + ".jpg";
            string oldStatus = Status;

            dbh.Progress += (sender, e) =>
            {
                Device.BeginInvokeOnMainThread(() => Progress = ((float)e.Position / (float)e.Total));
            };

            try
            {
                await Device.InvokeOnMainThreadAsync(() =>
                    Status = string.Format(AppResources.UploadingXImage, AppResources.Visible)
                );

                await dbh.SendFile(Drug.DrugId, ImageType.Visible, Drug.TrainingType, fileName, TrainingInfo.Source.VisStream);

                await Device.InvokeOnMainThreadAsync(() =>
                    Status = string.Format(AppResources.UploadingXImage, AppResources.Infrared)
                );

                await dbh.SendFile(Drug.DrugId, ImageType.Infrared, Drug.TrainingType, fileName, TrainingInfo.Source.IrStream);

                await Device.InvokeOnMainThreadAsync(() =>
                    Status = string.Format(AppResources.UploadingXImage, AppResources.Ultraviolet)
                );
                await dbh.SendFile(Drug.DrugId, ImageType.Ultraviolet, Drug.TrainingType, fileName, TrainingInfo.Source.UvStream);

                await Device.InvokeOnMainThreadAsync(() =>
                    Status = string.Format(AppResources.UploadingXImage, AppResources.Imprint)
                );

                await dbh.SendFile(Drug.DrugId, ImageType.Imprint, Drug.TrainingType, fileName, TrainingInfo.Source.ImpStream);

                await Device.InvokeOnMainThreadAsync(() => 
                {
                    Status = AppResources.Done;
                                      
                });
            
            }
            catch
            {
                return false;
            }

            return true;

        }

        public async Task TriggerTraining()
        {
            if (Drug == null) return;

            HttpClient cli = new HttpClient();
            var resp = await cli.GetAsync(string.Format(Constants.TriggerAutoMLTrainingURL, Drug.DrugId));

            var s = resp.Content.ReadAsStringAsync();

            resp.Dispose();
            cli.Dispose();
        }

        public float Progress
        {
            get => progress;
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        public DrugViewModel Drug
        {
            get => drug;
            set
            {
                if (drug == value) return;
                drug = value;
                OnPropertyChanged();
            }
        }

        public TrainingImageViewModel TrainingInfo
        {
            get => trainingInfo;
            set
            {
                if (trainingInfo == value) return;
                trainingInfo = value;
                OnPropertyChanged();
            }
        }

        public BluetoothConnection Connection
        {
            get => conn;
        }

        public BTDevice SelectedDevice
        {
            get => selDev;
            set
            {
                if (selDev == value) return;
                selDev = value;

                OnPropertyChanged();
                OnPropertyChanged("HasSelection");
            }
        }
        
        public bool HasSelection
        {
            get => selDev != null;
        }

        public ObservableCollection<BTDevice> Devices
        {
            get => devices;
            set
            {
                if (devices == value) return;

                devices = value;
                OnPropertyChanged();
            }
        }


    }
}
