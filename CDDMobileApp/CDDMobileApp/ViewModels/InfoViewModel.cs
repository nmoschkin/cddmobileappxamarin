﻿using CDDMobile.Localization.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using System.Linq;
using CDDMobile.Converters;
using System.Globalization;

namespace CDDMobile.ViewModels
{
    public class InfoPiece : ViewModelBase
    {

        private string value;

        public string Value
        {
            get => value;
            set
            {
                if (this.value == value) return;
                this.value = value;
                OnPropertyChanged();
            }
        }
    }

    public class ResourceReferenceAttribute : Attribute
    {

        static readonly string DefaultResId = "CDDMobile.Localization.Resources.AppResources";

        static readonly Lazy<ResourceManager> ResMgr = new Lazy<ResourceManager>(
            () => new ResourceManager(DefaultResId, IntrospectionExtensions.GetTypeInfo(typeof(AppResources)).Assembly));

        private ResourceManager LocalResMgr;

        public string ResourceId { get; private set; }

        public string ResourceKey { get; private set; }

        public ResourceReferenceAttribute(string resKey, string resId = null)
        {
            ResourceId = resId;
            ResourceKey = resKey;

            if (resId != null)
            {
                LocalResMgr = new ResourceManager(resId, Assembly.GetExecutingAssembly());
            }
        }

        public string Translate()
        {
            if (LocalResMgr != null)
            {
                return LocalResMgr.GetString(ResourceKey);
            }
            else
            {
                return ResMgr.Value.GetString(ResourceKey);
            }
        }

    }

    public class InfoViewModel : ViewModelBase
    {
        public ObservableCollection<InfoPiece> ItemInfo { get; } = new ObservableCollection<InfoPiece>();


        private string[] include;

        private string[] exclude;

        private object item;

        private string dtFormat = "d";

        public string DateTimeFormat
        {
            get => dtFormat;
            set
            {
                dtFormat = value;
                RefreshItem();

                OnPropertyChanged();
            }
        }

        public object Item
        {
            get => item;
            set
            {
                item = value;
                RefreshItem();

                OnPropertyChanged();
            }
        }


        public string[] Include
        {
            get => include;
            set
            {
                include = value;
        
                RefreshItem();
                OnPropertyChanged();
            }
        }

        public string[] Exclude
        {
            get => exclude;
            set
            {
                exclude = value;
                
                RefreshItem();
                OnPropertyChanged();
            }
        }

        public InfoViewModel()
        {

        }

        public InfoViewModel(object item, string[] includeProps = null, string[] excludeProps = null)
        {
            if (item == null) throw new ArgumentNullException();

            this.item = item;
            this.include = includeProps;
            this.exclude = excludeProps;

            RefreshItem();
        }

        public void RefreshItem()
        {
            ItemInfo.Clear();
            if (item == null) return;

            var props = Item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            bool inc;

            InfoPiece piece; 

            foreach (var prop in props)
            {
                inc = (include == null) || (include?.Contains(prop.Name) ?? true);
                inc &= (exclude == null) || !(exclude?.Contains(prop.Name) ?? true);
                EnumToTextConverter enumConv = new EnumToTextConverter();

                if (!inc) continue;

                ResourceReferenceAttribute res = prop.GetCustomAttribute<ResourceReferenceAttribute>();

                object val = prop.GetValue(item);

                piece = new InfoPiece
                {
                    Title = res?.Translate() ?? prop.Name,
                    Value = (val is Enum) ? 
                            (string)enumConv.Convert(val, typeof(string), null, CultureInfo.CurrentCulture) 
                            : (val is DateTime dt) ? 
                            dt.ToString(dtFormat) 
                            : val?.ToString() ?? ""
                };

                ItemInfo.Add(piece);
            }

        }

    }


}
