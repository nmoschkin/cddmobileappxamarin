﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace CDDMobile.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {

        protected static bool identifyMode = false;
        public static bool IdentifyMode
        {
            get => identifyMode;
            set
            {
                if (identifyMode == value) return;
                identifyMode = value;
            }
        }

        protected object selItem;

        public virtual object SelectedItem
        {
            get => selItem;
            set
            {
                if (selItem == value) return;
                selItem = value;
                OnPropertyChanged();
            }
        }
       
        protected string title;

        public virtual string Title
        {
            get => title;
            set
            {
                if (title == value) return;
                title = value;
                OnPropertyChanged();
            }
        }

        protected string status;

        public virtual string Status
        {
            get => status;
            set
            {
                if (status == value) return;
                status = value;
                OnPropertyChanged();
            }
        }


        protected bool confirm = false;
        public virtual bool Confirm
        {
            get => confirm;
            set
            {
                if (confirm == value) return;
                confirm = value;
                OnPropertyChanged();
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName()] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
