﻿using CDDMobile.Database.Models;
using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeploymentView : ContentView
    {
        DeploymentBucketViewModel vm;
        Pages.MainPage page;

        public DeploymentView()
        {
            InitializeComponent();

            vm = new DeploymentBucketViewModel();
            vm.BucketClicked += Vm_BucketClicked;
        
            BindingContext = vm;

            Buttons.AddButtonRange(new string[] { AppResources.Back });
            Buttons.ButtonClicked += Buttons_ButtonClicked;

        }
        protected override void OnParentSet()
        {
            var p = (Element)this;

            do
            {
                p = p.Parent;
            } while ((p != null) && !(p is Pages.MainPage));

            if (p == null) return;

            page = (Pages.MainPage)p;
            page.Appearing += Page_Appearing;
        }

        private async void Page_Appearing(object sender, EventArgs e)
        {
            await vm.RefreshBuckets();
        }

        private async void Vm_BucketClicked(object sender, EventArgs e)
        {

            var bucket = sender as DeploymentBucket;
            vm.SelectedItem = bucket;

            await Device.InvokeOnMainThreadAsync(async () => await page.Navigation.PushAsync(new Pages.MainPage(new DrugInfoView(vm))));


        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch(e.ButtonIndex)
            {
                case 0:

                    await page.Navigation.PopAsync();
                    break;
            }
        }
    }
}