﻿using Acr.UserDialogs.Forms;
using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UploadView : ContentView
    {
        private BluetoothViewModel vm;


        private DeploymentBucketViewModel dbvm;

        private CancellationTokenSource cToken;

        public UploadView(DeploymentBucketViewModel dbvm)
        {
            InitializeComponent();

            this.dbvm = dbvm;

            BindingContext = dbvm;

            dbvm.Title = AppResources.TransmitModels;
            dbvm.TransmitComplete += Dbvm_TransmitComplete;

            Buttons.Buttons[0].Text = AppResources.Transmit;

            Buttons2.AddButtonRange(new string[] { AppResources.Back, AppResources.MainMenu });

            Buttons.ButtonClicked += Buttons_ButtonClicked;
            Buttons2.ButtonClicked += Buttons2_ButtonClicked;
        }

        public UploadView(BluetoothViewModel btvm)
        {
            InitializeComponent();

            vm = btvm;

            BindingContext = vm;

            Buttons2.AddButtonRange(new string[] { AppResources.Back, AppResources.MainMenu });

            vm.Title = AppResources.UploadTrainingImages;

            Buttons.ButtonClicked += Buttons_ButtonClicked;
            Buttons2.ButtonClicked += Buttons2_ButtonClicked;
        }

        private void Dbvm_TransmitComplete(object sender, Network.ProgressEventArgs e)
        {
            if (e.State == Network.ConnectionState.Done)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Buttons.Buttons[0].IsEnabled = false;
                    Buttons2.Buttons[0].IsEnabled = false;
                    Buttons2.Buttons[1].Text = AppResources.MainMenu;

                    chkConfirm.IsEnabled = false;

                    cToken = null;
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    chkConfirm.IsEnabled = true;
                    Buttons.Buttons[0].IsEnabled = false;
                    Buttons2.Buttons[0].IsEnabled = true;
                    Buttons2.Buttons[1].Text = AppResources.MainMenu;

                    cToken = null;
                });

            }

        }

        private async void Buttons2_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch (e.ButtonIndex)
            {
                case 0:

                    await Navigation.PopAsync();
                    break;

                case 1:

                    if (cToken != null)
                    {
                        cToken.Cancel();
                        return;
                    }

                    await Navigation.PopToRootAsync();
                    break;
            }   
        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            
            switch (e.ButtonIndex)
            {
                case 0:

                    if (dbvm != null)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {

                            Buttons.Buttons[0].IsEnabled = false;
                            Buttons2.Buttons[0].IsEnabled = false;
                            Buttons2.Buttons[1].Text = AppResources.Cancel;

                            chkConfirm.IsEnabled = false;

                        });

                        await Task.Yield();
                        cToken = new CancellationTokenSource();
                        
                        dbvm.BeginTransmitTrainingModel(cToken.Token);


                    }
                    else if (vm != null)
                    {
                        
                        if (e.ButtonText == AppResources.BeginTraining)
                        {
                            var cfg = new ConfirmConfig()
                            {
                                Message = AppResources.AskTriggerTraining,
                                OkLabel = AppResources.Yes,
                                CancelLabel = AppResources.No,
                                Title = AppResources.Training
                            };

                            var dlg = new UserDialogs();

                            var conf = await dlg.Confirm(cfg);

                            if (conf)
                            {
                                await vm.TriggerTraining();
                                Buttons2.Buttons[1].Text = AppResources.MainMenu;


                                await Navigation.PopToRootAsync();
                            }
                            
                            return;
                        }

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Buttons.Buttons[0].IsEnabled = false;
                            Buttons2.Buttons[0].IsEnabled = false;
                            Buttons2.Buttons[1].IsEnabled = false;

                            chkConfirm.IsEnabled = false;

                        });

                        await Task.Yield();

                        if (!await vm.UploadTrainingImages())
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                chkConfirm.IsEnabled = true;

                                Buttons.Buttons[0].Text = AppResources.Upload;

                                Buttons.Buttons[0].IsEnabled = false;
                                Buttons2.Buttons[0].IsEnabled = true;
                                Buttons2.Buttons[1].IsEnabled = true;
                            });
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                chkConfirm.IsEnabled = true;

                                Buttons.Buttons[0].IsEnabled = false;
                                Buttons.Buttons[0].Text = AppResources.BeginTraining;

                                Buttons2.Buttons[0].IsEnabled = true;
                                Buttons2.Buttons[1].IsEnabled = true;

                                cToken = null;

                            });
                        }

                   
                    }

                    break;

            }

        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (chkConfirm.IsEnabled == false) return;
            chkConfirm.IsChecked = !chkConfirm.IsChecked;
        }

    }
}