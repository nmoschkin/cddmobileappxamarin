﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using CDDMobile.Network;

using Acr.UserDialogs.Forms;

namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BluetoothView : ContentView
    {
        private BluetoothViewModel vm;
        bool openSettingsMode = false;

        public BluetoothView()
        {
            InitializeComponent();
            Buttons.AddButton(AppResources.Connect);
            Buttons.ButtonClicked += Buttons_ButtonClicked;

            vm = new BluetoothViewModel();
            vm.Title = AppResources.Bluetooth;

            BindingContext = vm;
            // Buttons.Buttons[0].IsEnabled = vm.HasSelection;

            vm.PropertyChanged += Vm_PropertyChanged;

        }

        private async void Page_Appearing(object sender, EventArgs e)
        {
            await RefreshBluetooth();
        }

        private void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(BluetoothViewModel.HasSelection))
            {
                // Buttons.Buttons[0].IsEnabled = vm.HasSelection;
            }
        }

        protected async Task RefreshBluetooth()
        {
            Acr.UserDialogs.Forms.UserDialogs dlg;
            Acr.UserDialogs.Forms.ConfirmConfig cfg;
            Acr.UserDialogs.Forms.AlertConfig cfg2;

            if (await BluetoothConnection.CheckAdapterStateAsync(true))
            {
                vm.RefreshDeviceList();

                if (vm.Devices.Count == 0)
                {
                    dlg = new Acr.UserDialogs.Forms.UserDialogs();
                    cfg = new Acr.UserDialogs.Forms.ConfirmConfig()
                    {
                        Message = AppResources.ErrorNoDeviceAndAskOpenBluetoothSettings,
                        Title = AppResources.Bluetooth,
                        OkLabel = AppResources.Yes,
                        CancelLabel = AppResources.No
                    };

                    var res = await dlg.Confirm(cfg);

                    if (res)
                    {
                        vm.Connection.OpenSettings();
                    }
                    else
                    {
                        cfg2 = new Acr.UserDialogs.Forms.AlertConfig()
                        {
                            Message = AppResources.ErrorNotPaired,
                            Title = AppResources.Error,
                            OkLabel = AppResources.OK
                        };

                        await dlg.Alert(cfg2);

                        openSettingsMode = true;
                        Buttons.Buttons[0].Text = AppResources.OpenBluetoothSettings;
                    }
                } 
                else
                {
                    openSettingsMode = false;
                    Buttons.Buttons[0].Text = AppResources.Connect;
                }

            }
            else
            {

                dlg = new Acr.UserDialogs.Forms.UserDialogs();
                cfg2 = new Acr.UserDialogs.Forms.AlertConfig()
                {
                    Message = AppResources.ErrorNotPaired,
                    Title = AppResources.Error,
                    OkLabel = AppResources.OK
                };

                await dlg.Alert(cfg2);

                throw new BluetoothUnavailableException(AppResources.ErrorBluetoothUnavailable);
            }

        }

        protected override void OnParentSet()
        {
            Pages.MainPage page;
            var p = (Element)this;

            do
            {
                p = p.Parent;
            } while ((p != null) && !(p is Pages.MainPage));

            if (p == null) return;

            page = (Pages.MainPage)p;
            page.Appearing += Page_Appearing;
        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch (e.ButtonIndex)
            {
                case 0:

                    if (openSettingsMode)
                    {
                        vm.Connection.OpenSettings();
                        return;
                    }

                    if (!vm.HasSelection)
                    {
                        var dlg = new UserDialogs();
                        var cfg = new AlertConfig
                        {
                            OkLabel = AppResources.OK,
                            Title = AppResources.Bluetooth,
                            Message = AppResources.WarnSelectDeviceToContinue
                        };

                        await dlg.Alert(cfg);
                        return;
                    }

                    Settings.DeviceName = vm.SelectedDevice.Name;
                    ((App)Application.Current).MainPage = new NavigationPage(new Pages.MainPage());

                    break;
            }
        }

        private void devList_SelectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }
    }
}