﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using CDDMobile.Network;

using Acr.UserDialogs;
using CDDMobile.Database.ViewModels;
using System.Runtime.InteropServices;
using CDDMobile.Database.Models;

namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CaptureView : ContentView
    {

        private Pages.MainPage page;
        private BluetoothViewModel vm;

        public CaptureView(DrugViewModel drug)
        {
            InitializeComponent();

            SubHeading.BackgroundColor = Color.White;
            SubHeading.TextColor = Color.Black;
            SubHeading.FontAttributes = FontAttributes.None;


            vm = new BluetoothViewModel();

            if (ViewModelBase.IdentifyMode)
            {

                vm.Title = AppResources.Test;

                Buttons.AddButtonRange(new string[] { AppResources.Test, AppResources.Back });
                Buttons.ButtonClicked += Buttons_ButtonClicked;

                SubHeading.Text = AppResources.ClickTestToBegin;

            }
            else
            {
                vm.Title = AppResources.Capture;

                Buttons.AddButtonRange(new string[] { AppResources.Capture, AppResources.Back });
                Buttons.ButtonClicked += Buttons_ButtonClicked;

                SubHeading.Text = AppResources.ClickCaptureToBegin;
            }

            vm.Drug = drug;
            vm.Connection.NotifyProgress += Connection_NotifyProgress;

            BindingContext = vm;
        }


        protected override void OnParentSet()
        {
            var p = (Element)this;

            do
            {
                p = p.Parent;
            } while ((p != null) && !(p is Pages.MainPage));

            if (p == null) return;

            page = (Pages.MainPage)p;
            page.Appearing += Page_Appearing;
        }

        private void Page_Appearing(object sender, EventArgs e)
        {

            vm.TrainingInfo = null;
            progress.IsVisible = true;
            images.IsVisible = false;

            if (ViewModelBase.IdentifyMode)
            {

                vm.Title = AppResources.Test;
                SubHeading.Text = AppResources.ClickTestToBegin;

            }
            else
            {
                vm.Title = AppResources.Capture;
                SubHeading.Text = AppResources.ClickCaptureToBegin;
            }

        }

        protected override void OnSizeAllocated(double width, double height)
        {
            ImageGrid.WidthRequest = ImageGrid.Height;

            base.OnSizeAllocated(width, height);
        }

        private void Connection_NotifyProgress(object sender, ProgressEventArgs e)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {

                switch (e.State)
                {

                    case ConnectionState.Connected:

                        SubHeading.Text = AppResources.Connected;
                        break;

                    case ConnectionState.Connecting:

                        SubHeading.Text = AppResources.Connecting;
                        break;

                    case ConnectionState.Disconnected:

                        SubHeading.Text = AppResources.Disconnected;
                        break;

                    case ConnectionState.Receiving:

                        SubHeading.Text = AppResources.Receiving;
                        break;

                    case ConnectionState.Transmitting:

                        SubHeading.Text = AppResources.Transmitting;
                        break;
                }

                progress.Progress = (e.Position / e.Total);

            });

        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch (e.ButtonIndex)
            {
                case 0:

                    if (Buttons.Buttons[0].Text == AppResources.Next)
                    {
                        Buttons.Buttons[0].Text = AppResources.Capture;
                        await page.Navigation.PushAsync(new Pages.MainPage(new UploadView(vm)));

                        return;
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {

                        SubHeading.BackgroundColor = Color.White;
                        SubHeading.TextColor = Color.Black;
                        SubHeading.FontAttributes = FontAttributes.None;

                        progress.Progress = 0;
                        progress.IsVisible = true;
                        images.IsVisible = false;

                        Buttons.Buttons[0].IsEnabled = false;
                        Buttons.Buttons[1].IsEnabled = false;

                    });

                    CommandResult? res = await vm.Capture(ViewModelBase.IdentifyMode, vm.Drug.DrugId);


                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Buttons.Buttons[0].IsEnabled = true;
                        Buttons.Buttons[1].IsEnabled = true;

                    });


                    Device.BeginInvokeOnMainThread(() =>
                    {

                        switch (res)
                        {

                            case CommandResult.CaptureOnly:

                                SubHeading.Text = AppResources.CapturedImages;
                                progress.IsVisible = false;
                                images.IsVisible = true;

                                Buttons.Buttons[0].Text = AppResources.Next;

                                break;

                            case CommandResult.Authentic:


                                SubHeading.Text = AppResources.Authentic;

                                SubHeading.BackgroundColor = Color.Green;
                                SubHeading.TextColor = Color.White;
                                SubHeading.FontAttributes = FontAttributes.Bold;

                                progress.IsVisible = false;
                                images.IsVisible = true;

                                break;

                            case CommandResult.Counterfeit:


                                SubHeading.Text = AppResources.Counterfeit;

                                SubHeading.BackgroundColor = Color.Red;
                                SubHeading.TextColor = Color.White;
                                SubHeading.FontAttributes = FontAttributes.Bold;

                                progress.IsVisible = false;
                                images.IsVisible = true;

                                break;


                            case null:
                            default:

                                progress.Progress = 0;
                                SubHeading.Text = AppResources.ErrorImageCapture;

                                break;
                        }

                    });

                    break;

                case 1:


                    await page.Navigation.PopAsync();
                    break;
            }
        }
    }
}