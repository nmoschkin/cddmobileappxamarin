﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Acr.UserDialogs;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CDDMobile.Localization.Resources;
using Acr.UserDialogs;
using CDDMobile.Database.Models;
using Newtonsoft.Json;
using Telerik.XamarinForms.DataControls.TreeView;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using CDDMobile.Database;
using CDDMobile.Pages;
using CDDMobile.ViewModels;

namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainView : ContentView
    {
        private MainPageViewModel vm = new MainPageViewModel();

        public MainView()
        {
            InitializeComponent();

            Buttons.AddButtonRange(new string[] { AppResources.Train, AppResources.Deploy, AppResources.Test });
            Buttons.ButtonClicked += Buttons_ButtonClicked;

            vm.Title = AppResources.MainMenu;
            BindingContext = vm;
        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch (e.ButtonIndex)
            {
                case 0:

                    ViewModelBase.IdentifyMode = false;
                    await Navigation.PushAsync(new MainPage(new DrugTestView()));
                    break;

                case 1:

                    await Navigation.PushAsync(new MainPage(new DeploymentView()));
                    break;

                case 2:

                    ViewModelBase.IdentifyMode = true;
                    await Navigation.PushAsync(new MainPage(new DrugTestView()));
                    break;
            }

        }
    }
}