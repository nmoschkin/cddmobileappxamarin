﻿using CDDMobile.Controls;
using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using CDDMobile.Database.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DrugTestView : ContentView
    {
        private DrugSearchViewModel vm;

        public DrugTestView()
        {
            InitializeComponent();

            vm = new DrugSearchViewModel();

            if (ViewModelBase.IdentifyMode)
            {
                var g = PickerBox.Parent as Grid;
                PickerBox.IsVisible = false;
                vm.Title = AppResources.Testing;
            }

            else
            {
                vm.Title = AppResources.Training;
            }
            
            PickerBox.ItemsSource = new string[] { AppResources.None, AppResources.Authentic, AppResources.Counterfeit };
            Buttons.AddButtonRange(new string[] { AppResources.Next, AppResources.Back });

            Buttons.ButtonClicked += Buttons_ButtonClicked;

            BindingContext = vm;

        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch (e.ButtonIndex)
            {
                case 0:

                    var cfg = new Acr.UserDialogs.Forms.AlertConfig()
                    {
                        Title = AppResources.Error,
                        OkLabel = AppResources.OK
                    };

                    var dlg = new Acr.UserDialogs.Forms.UserDialogs();

                    if (!(vm.SelectedItem is DrugViewModel))
                    {
                        cfg.Message = AppResources.ErrorSelectDrug;

                        await dlg.Alert(cfg);

                        DrugBox.Focus();

                        return;
                    }
                    else if (!ViewModelBase.IdentifyMode && vm.TrainingType == Database.TrainingType.None)
                    {
                        cfg.Message = AppResources.ErrorSelectTrainingType;

                        await dlg.Alert(cfg);

                        PickerBox.Focus();

                        return;
                    }
                    else
                    {
                        var dvm = vm.SelectedItem as DrugViewModel;
                        
                        dvm.TrainingType = ViewModelBase.IdentifyMode ? Database.TrainingType.None : vm.TrainingType;

                        await Navigation.PushAsync(new Pages.MainPage(new DrugInfoView(dvm)));
                    }

                    break;

                case 1:
                    await Navigation.PopAsync();
                    break;

            }
        }
    }
}
