﻿using CDDMobile.Database.Models;
using CDDMobile.Database.ViewModels;
using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;



namespace CDDMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DrugInfoView : ContentView
    {

        InfoViewModel vm;

        DeploymentBucketViewModel dbvm;


        public DrugInfoView(DeploymentBucketViewModel dbvm) 
        {

            InitializeComponent();

            Buttons.AddButtonRange(new string[] { AppResources.Next, AppResources.Back });
            Buttons.ButtonClicked += Buttons_ButtonClicked;

            this.dbvm = dbvm;

            var selBucket = dbvm.SelectedItem as DeploymentBucket;

            var drug = selBucket.Drug;

            vm = new InfoViewModel(drug, null, new string[] { "Source", "ImageUrl", "TrainingStatus", "TrainingAlgorithm", "DrugId" })
            {
                Title = AppResources.Training
            };

            BindingContext = vm;

        }


        public DrugInfoView(DrugViewModel drug)
        {
            InitializeComponent();

            Buttons.AddButtonRange(new string[] { AppResources.Next, AppResources.Back });
            Buttons.ButtonClicked += Buttons_ButtonClicked;

            vm = new InfoViewModel(drug, null, new string[] { "Source", "ImageUrl", "TrainingStatus", "TrainingAlgorithm", "DrugId" })
            {
                Title = AppResources.Training
            };

            BindingContext = vm;


        }

        private async void Buttons_ButtonClicked(object sender, Controls.ButtonClickedEventArgs e)
        {
            switch (e.ButtonIndex)
            {
                case 0:

                    if (dbvm != null)
                    {
                        await Navigation.PushAsync(new Pages.MainPage(new UploadView(dbvm)));

                    }
                    else
                    {
                        await Navigation.PushAsync(new Pages.MainPage(new CaptureView(vm.Item as DrugViewModel)));
                    }

                    break;

                case 1:

                    await Navigation.PopAsync();
                    break;
            }
        }
    }

}