﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Localization
{
    public enum HourFormat : int
    {
        Hour12,
        Hour24
    }

}
