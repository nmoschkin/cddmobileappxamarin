﻿using System.Globalization;
using Xamarin.Essentials;

namespace CDDMobile.Localization
{
    public static class LocalizationSettings
    {
        // Language Settings
        private const string LanguageKey = "Language";
        private const string LanguageDefault = "en";

        private const string IsMerticKey = "IsMetric";
        private const bool IsMerticDefault = false;

        private const string ClockTypeKey = "ClockType";
        private static readonly int ClockTypeDefault = (int)HourFormat.Hour12;

        public static string Language
        {

            get => Preferences.Get(LanguageKey, LanguageDefault);
            set
            {
                Preferences.Remove(LanguageKey);
                Preferences.Set(LanguageKey, value);
            }
        }

        public static CultureInfo UserCulture
        {
            get
            {
                try
                {
                    if (Language == null) return null;

                    var x = new CultureInfo(Language);
                    return x;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                Language = value.TwoLetterISOLanguageName;
            }
        }

        public static bool IsMetric
        {
            get => Preferences.Get(IsMerticKey, IsMerticDefault);
            set => Preferences.Set(IsMerticKey, value);
        }

        public static HourFormat ClockType
        {
            get => (HourFormat)Preferences.Get(ClockTypeKey, ClockTypeDefault);
            set => Preferences.Set(ClockTypeKey, (int)value);
        }

    }
}
