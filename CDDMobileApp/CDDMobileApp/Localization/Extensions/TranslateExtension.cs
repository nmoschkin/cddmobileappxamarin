﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CDDMobile.Localization;
using CDDMobile.Localization.Resources;

namespace CDDMobile.Localization
{
    // You exclude the 'Extension' suffix when using in XAML
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        CultureInfo ci = null;
        const string ResourceId = "CDDMobile.Localization.Resources.AppResources";

        static readonly Lazy<ResourceManager> ResMgr = new Lazy<ResourceManager>(
            () => new ResourceManager(ResourceId, IntrospectionExtensions.GetTypeInfo(typeof(AppResources)).Assembly));

        public string Text { get; set; }

        public TranslateExtension()
        {
            ci = ((App)App.Current).ActiveCulture;
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return string.Empty;

            string translation = null;


            try
            {
                translation = ResMgr.Value.GetString(Text, ci);
            }
            catch
            {
                try
                {
                    translation = ResMgr.Value.GetString(Text, new CultureInfo("en")); // default to english
                }
                catch
                {
                    translation = "bad translation for " + Text;
                }
            }


            if (translation == null)
            {
                ArgumentException ex = new ArgumentException(
                    string.Format("Key '{0}' was not found in resources '{1}' for culture '{2}'.", Text, ResourceId, ci.Name),
                    "Text");
                App.TrackError(ex);
#if DEBUG
                throw ex;
#else
                try
                {
                    translation = ResMgr.Value.GetString(Text, new CultureInfo("en")); // default to english
                }
                catch
                {
                    translation = Text; // HACK: returns the key, which GETS DISPLAYED TO THE USER
                }
#endif
            }
            return translation;
        }
    }

    public class FlowExtension : IMarkupExtension
    {
        public string Text { get; set; }

        public FlowExtension()
        {
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return ((App)(App.Current)).FlowDirection;
        }
    }


}
