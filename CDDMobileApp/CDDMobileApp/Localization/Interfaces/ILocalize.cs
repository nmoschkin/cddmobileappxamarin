﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace CDDMobile.Localization
{

    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }

}
