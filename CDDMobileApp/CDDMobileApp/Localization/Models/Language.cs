﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("CDDMobile")]
[assembly: InternalsVisibleTo("CDDMobile.Android")]
[assembly: InternalsVisibleTo("CDDMobile.iOS")]
namespace CDDMobile.Localization
{
    public class Language
    {
        public string Name { get; internal set; }
        public string Code { get; internal set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Languages : List<Language>
    {
        public Languages()
        {
            this.Add(new Language() { Name = "English", Code = "en" });
            //this.Add(new Language() { Name = "Español", Code = "es" });
            //this.Add(new Language() { Name = "Português", Code = "pt" });
            //this.Add(new Language() { Name = "Italiano", Code = "it" });
            //this.Add(new Language() { Name = "Français", Code = "fr" });
            //this.Add(new Language() { Name = "العربية", Code = "ar" });
            //this.Add(new Language() { Name = "עברית", Code = "he" });
        }
    }

}
