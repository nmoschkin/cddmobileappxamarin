﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CDDMobile.Localization;
using CDDMobile.Localization.Resources;
using System.Threading;
using System.Globalization;
using Xamarin.Essentials;
using System.Linq;
using Microsoft.AppCenter.Crashes;
using System.ComponentModel;
using CDDMobile.Network;
using CDDMobile.Views;
using CDDMobile.Converters;
using CDDMobile.Database;

namespace CDDMobile
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            InitLanguage();

            


            if (!BluetoothConnection.CheckAdapterState() || string.IsNullOrEmpty(Settings.DeviceName))
            {
                MainPage = new NavigationPage(new Pages.MainPage(new BluetoothView()));
            }
            else
            {
                MainPage = new NavigationPage(new Pages.MainPage());
            }

        }

        protected override void OnStart()
        {
        

        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        internal static void TrackError(Exception ex, System.Collections.Generic.IDictionary<string, string> properties = null)
        {
            if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
            {
#pragma warning disable CS0618
                Crashes.TrackError(ex, properties);
#pragma warning restore
            }
        }

        #region Internationalization

        private CultureInfo mci;
        private CultureInfo oscult;

        public FlowDirection FlowDirection
        {
            get
            {
                switch (mci.TwoLetterISOLanguageName)
                {
                    case "ar":
                    case "he":
                        return FlowDirection.RightToLeft;

                    default:
                        return FlowDirection.LeftToRight;
                }
            }
        }

        private void InitLanguage()
        {

            CultureInfo ci;

            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            }
            else
            {
                ci = CultureInfo.CurrentCulture;
            }

            // For the lifetime of the program
            oscult = ci;

            if (VersionTracking.IsFirstLaunchEver)
            { // see if we support this language else fall back to default "en"
                Languages supportedLangs = new Languages();
                if (!supportedLangs.Any(l => l.Code == ci.TwoLetterISOLanguageName))
                { // we don't then
                    ActiveCulture = LocalizationSettings.UserCulture;
                }
                else
                {
                    ActiveCulture = ci;
                    LocalizationSettings.Language = ci.TwoLetterISOLanguageName;

                    SetCultureDefaults(ci);
                }
            }
            else
            {
                ActiveCulture = LocalizationSettings.UserCulture;
                if (ActiveCulture == null)
                {
                    ResetCulture();
                }

            }

            // to test the internationalization
            // ActiveCulture = oscult = ci = new CultureInfo("fr");

        }

        public CultureInfo ActiveCulture
        {
            get
            {
                return mci;
            }
            set
            {
                mci = value;

                AppResources.Culture = mci; // set the RESX for resource localization

                CultureInfo.DefaultThreadCurrentCulture = mci;
                CultureInfo.DefaultThreadCurrentUICulture = mci;

                CultureInfo.CurrentCulture = mci;

                Thread.CurrentThread.CurrentUICulture = mci;
                Thread.CurrentThread.CurrentCulture = mci;

            }
        }

        public void SetCultureDefaults(CultureInfo cult)
        {

            if (cult.Name == "en-US")
            {
                LocalizationSettings.IsMetric = false;
            }
            else
            {
                LocalizationSettings.IsMetric = true;
            }

            if (cult.DateTimeFormat.PMDesignator == "")
            {
                LocalizationSettings.ClockType = HourFormat.Hour24;
            }
            else
            {
                LocalizationSettings.ClockType = HourFormat.Hour12;
            }
        }

        public void ResetCulture()
        {
            CultureInfo ci = oscult;

            SetCultureDefaults(ci);
            LocalizationSettings.UserCulture = ci;
        }
        #endregion

    }
}
