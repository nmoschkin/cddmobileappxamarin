﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CDDMobile.Database.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using CDDMobile.Database.Models;
using CDDMobile.ViewModels;
using System.Collections.ObjectModel;
using CDDMobile.Views;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Storage;
using Xamarin.Essentials;
using System.Linq.Expressions;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json.Serialization;
using CDDMobile.Localization.Resources;
using CDDMobile.Network;
using System.Diagnostics;
using System.Net.Http;
using System.IO;

namespace CDDMobile.Database
{
    public class DrugDBHelper
    {

        public delegate void ProgressEvent(object sender, ProgressEventArgs e);

        public event ProgressEvent Progress;

        public FirebaseClient Client { get; private set; } = null;

        public FirebaseStorage Storage { get; private set; } = null;

        public bool CfddBucket { get; private set; }

        public bool SignedIn { get; private set; }

        public async Task<bool> SignIn()
        {
            CfddBucket = false;
            return await SignIn(Constants.ApiKey, Constants.Username, Constants.Password, Constants.DatabaseURL, Constants.StorageBucket);
        }

        public async Task<bool> SignIn(string apiKey, string username, string password, string database = null, string storageBucket = null) 
        {
            try
            {
                SignedIn = false;

                Thread.Sleep(10);

                var authProvider = new FirebaseAuthProvider(new FirebaseConfig(apiKey));
                var auth = await authProvider.SignInWithEmailAndPasswordAsync(username, password);
                
                if (database != null)
                    Client = new FirebaseClient(
                                        database,
                                        new FirebaseOptions
                                        {
                                            AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken),
                                        });
                
                if (storageBucket != null)
                    Storage = new FirebaseStorage(
                                    storageBucket,
                                    new FirebaseStorageOptions()
                                    {
                                        AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken),
                                    });

                SignedIn = true;
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                SignedIn = false;
                return false;
            }

        }

        #region CFDD Bucket

        public async Task<string[]> GetTrainedModels(BucketList bucket = null)
        {
            if (bucket == null) bucket = await GetCFDDBucket();
            var output = new List<string>();

            foreach (var item in bucket.Items)
            {
                string s;
                s = item.GetDrug();

                output.Add(s);
            }

            output.Sort();

            int i, c = output.Count;

            for (i = c - 1; i >= 0; i--)
            {
                if (i > 0 && output[i - 1] == output[i]) output.RemoveAt(i);
            }

            return output.ToArray();
        }

        public async Task<DeploymentBucket> GetDeploymentBucket(string drugId, BucketList bucket = null)
        {
            if (bucket == null) bucket = await GetCFDDBucket();

            var deploy = new DeploymentBucket();

            var files = new List<string>();

            drugId = drugId.ToLower();

            deploy.DrugId = drugId;
            deploy.Timestamp = DateTime.Now;

            foreach (var item in bucket.Items)
            {
                string s;

                s = item.GetDrug();

                if (s == drugId && Path.GetExtension(item.Name) == ".tflite")
                    files.Add(item.Name);
            }

            deploy.Files = files.ToArray();
            return deploy;


        }

        public async Task<Stream> DownloadBucketItem(string item)
        {
            item = item.Replace("/", "%2F");

            var reqUrl = $"https://firebasestorage.googleapis.com/v0/b/{Constants.AutoMLTrainingBucket}/o/{item}?alt=media";

            Uri uri;

            if (!SignedIn || !CfddBucket)
            {
                if (!await SignIn(Constants.TFApiKey, Constants.Username, Constants.Password, null, Constants.AutoMLTrainingBucket + ".appspot.com")) return null;
                CfddBucket = true;
            }

            HttpClient cli;
            HttpResponseMessage resp;

            MemoryStream contents;

            uri = new Uri(reqUrl);

            try
            {

                cli = await Storage.Options.CreateHttpClientAsync();
                cli.Timeout = new TimeSpan(0, 0, 10);
                
                resp = await cli.GetAsync(uri);

                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    contents = new MemoryStream();
                    await resp.Content.CopyToAsync(contents);
                    contents.Seek(0, SeekOrigin.Begin);

                    return contents;
                }
            }
            catch
            {

            }

            return null;

        }

        public async Task<BucketList> GetCFDDBucket()
        {
            var reqUrlPrefix = $"https://firebasestorage.googleapis.com/v0/b/{Constants.AutoMLTrainingBucket}/o";
            var reqUrl = $"{reqUrlPrefix}?delimiter=/";

            var uri = new Uri(reqUrl);

            if (!SignedIn || !CfddBucket)
            {
                if (!await SignIn(Constants.TFApiKey, Constants.Username, Constants.Password, null, Constants.AutoMLTrainingBucket + ".appspot.com")) return null;
                CfddBucket = true;
            }

            BucketList bpass1;
            BucketList outBucket = new BucketList();

            outBucket.Items = new List<BucketItem>();

            BucketList bpass2;

            HttpClient cli;
            HttpResponseMessage resp;

            try
            {
                
                cli = await Storage.Options.CreateHttpClientAsync();
                cli.Timeout = new TimeSpan(0, 0, 10);

                resp = await cli.GetAsync(uri);

                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var json = await resp.Content.ReadAsStringAsync();
                    bpass1 = JsonConvert.DeserializeObject<BucketList>(json);


                    foreach (var p in bpass1.Prefixes)
                    {
                        reqUrl = $"{reqUrlPrefix}?prefix=" + p.Replace("/", "%2F") + "models%2Fmodel-export%2Ficn%2F";
                        uri = new Uri(reqUrl);

                        resp = await cli.GetAsync(uri);

                        if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            json = await resp.Content.ReadAsStringAsync();
                            bpass2 = JsonConvert.DeserializeObject<BucketList>(json);

                            outBucket.Items.AddRange(bpass2.Items);
                        }

                    }

                }
            }
            catch
            {
                return null;
            }

            return outBucket;    

        }

        #endregion

        #region Storage

        public async Task<string> SendFile(string drugId, ImageType imageType, TrainingType trainingType, string fileName, System.IO.Stream stream)
        {

            if (!SignedIn || CfddBucket)
            {
                if (!await SignIn()) return null;
            }

            if (trainingType == TrainingType.None) throw new ArgumentException();

            string[] its = new string[] { "", "vis", "uv", "ir", "imp" };

            string it = its[(int)imageType];

            stream.Seek(0, System.IO.SeekOrigin.Begin);

            // Constructr FirebaseStorage, path to where you want to upload the file and Put it there
            var task = Storage
                .Child("images")
                .Child(drugId)
                .Child(trainingType.ToString().ToLower())
                .Child(it)
                .Child(fileName)
                .PutAsync(stream);

            // Track progress of the upload
            task.Progress.ProgressChanged += (s, e) => Progress?.Invoke(this, new ProgressEventArgs((int)e.Position, (int)e.Length, ConnectionState.Transmitting));

            // await the task to wait until upload completes and get the download url
            var downloadUrl = await task;
            return downloadUrl;
        }


        #endregion


        #region Database

        public async Task<ObservableCollection<TrainingViewModel>> GetTrainingViewModels()
        {
            var models = await GetTrainingData();
            var col = new ObservableCollection<TrainingViewModel>();

            foreach (var kv in models)
            {
                col.Add(new TrainingViewModel(kv.Value));
            }

            return col;
        }


        public async Task<Dictionary<string, Training>> GetTrainingData()
        {
            if (!SignedIn || CfddBucket)
            {
                if (!await SignIn()) return null;
            }
            var trainingData = await Client
              .Child("TrainingData")
              .OnceAsync<Training>();

            Dictionary<string, Training> training = new Dictionary<string, Training>();

            foreach (var t in trainingData)
            {
                training.Add(t.Key, t.Object);
            }

            return training;    

        }


        public async Task<ObservableCollection<DrugViewModel>> GetDrugViewModels()
        {
            var models = await GetDrugData();
            var col = new ObservableCollection<DrugViewModel>();

            foreach (var kv in models)
            {
                col.Add(new DrugViewModel(kv.Value));
            }

            return col;
        }

        public async Task<Dictionary<string, Drug>> GetDrugData()
        {
            if (!SignedIn || CfddBucket)
            {
                if (!await SignIn()) return null;
            }

            Dictionary<string, Drug> drug = new Dictionary<string, Drug>();

            try
            {
                var drugData = await Client
                  .Child("drugs")
                  .OnceAsync<Drug>();

                foreach (var t in drugData)
                {
                    drug.Add(t.Key, t.Object);
                }


            }
            catch (FirebaseException e) 
            {
                try
                {
                    var cfg = new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        Error = new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>(async (sender, e2) =>
                        {

                            var dlg = new Acr.UserDialogs.Forms.UserDialogs();
                            var ucfg = new Acr.UserDialogs.Forms.AlertConfig()
                            {
                                Message = e2.ToString(),
                                Title = AppResources.Error,
                                OkLabel = AppResources.OK
                            };


                            await dlg.Alert(ucfg);


                        })
                    };

                    var e_drugs = JsonConvert.DeserializeObject<Dictionary<string, Drug>>(e.ResponseData, cfg);

                    if (e_drugs != null) return e_drugs;
                }
                catch
                {

                }


            }

            return drug;
        }


        public async Task<ObservableCollection<DeploymentViewModel>> GetDeploymentViewModels()
        {
            var models = await GetDeploymentData();
            var col = new ObservableCollection<DeploymentViewModel>();

            foreach (var kv in models)
            {
                col.Add(new DeploymentViewModel(kv.Value));
            }

            return col;
        }


        public async Task<Dictionary<string, Deployment>> GetDeploymentData()
        {
            if (!SignedIn || CfddBucket)
            {
                if (!await SignIn()) return null;
            }
            var deploymentData = await Client
              .Child("deploymentData")
              .OnceAsync<Deployment>();

            Dictionary<string, Deployment> deployment = new Dictionary<string, Deployment>();

            foreach (var t in deploymentData)
            {
                deployment.Add(t.Key, t.Object);
            }

            return deployment;

        }

        #endregion

    }


    


}
