﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.XamarinForms.Input.AutoComplete;

using CDDMobile.Database.ViewModels;

namespace CDDMobile.Filters
{
    public class DrugSearchFilter : IAutoCompleteFilter
    {
        public bool Filter(object item, string searchText, CompletionMode completionMode)
        {
            if (item == null || searchText == null) return false;

            string[] comps;

            if (item is DrugViewModel drug)
            {
                comps = new string[] { drug.BrandName, drug.GenericName, drug.BatchNumber, drug.DosageAmount, drug.Manufacturer };
            }
            else if (item is TrainingViewModel training)
            {
                comps = new string[] { training.GenericName, training.BatchNumber, training.DosageAmount };
            }
            else if (item is DeploymentViewModel deploy)
            { 
                comps = new string[] { deploy.BrandName, deploy.GenericName };
            }
            else
            {
                return false;
            }

            foreach (var c in comps)
            {
                if (c == null) continue;
                if (c.Contains(searchText, StringComparison.OrdinalIgnoreCase)) return true;
            }

            return false;
        }
    }
}
