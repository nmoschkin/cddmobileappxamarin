﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Database.Models
{

    public enum ImageType
    {
        [JsonProperty("vis")]
        Visible = 1,

        [JsonProperty("uv")]
        Ultraviolet,

        [JsonProperty("ir")]
        Infrared,
        
        [JsonProperty("imp")]
        Imprint
    }

    public class Image
    {
        [JsonProperty("F1_score")]
        public float F1_score { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("precision")]
        public float Precision { get; set; }

        [JsonProperty("recall")]
        public float Recall { get; set; }

        [JsonProperty("type")]
        public ImageType Type { get; set; }

    }



}
