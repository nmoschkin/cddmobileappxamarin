﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CDDMobile.Database.Models;

namespace CDDMobile.Database.ViewModels
{
    public class DeploymentViewModel : INotifyPropertyChanged
    {
        public Deployment Source { get; private set; }


        public DeploymentViewModel()
        {
            Source = new Deployment();
        }

        public DeploymentViewModel(Deployment source)
        {
            if (source == null) throw new ArgumentNullException();

            Source = source;
        }


        public string BrandName
        {
            get => Source.BrandName;
            set
            {
                if (Source.BrandName == value) return;
                Source.BrandName = value;
                OnPropertyChanged();
            }
        }
        public string DeploymentStatus
        {
            get => Source.DeploymentStatus;
            set
            {
                if (Source.DeploymentStatus == value) return;
                Source.DeploymentStatus = value;
                OnPropertyChanged();
            }
        }
        public string DrugId
        {
            get => Source.DrugId;
            set
            {
                if (Source.DrugId == value) return;
                Source.DrugId = value;
                OnPropertyChanged();
            }
        }
        public string GenericName
        {
            get => Source.GenericName;
            set
            {
                if (Source.GenericName == value) return;
                Source.GenericName = value;
                OnPropertyChanged();
            }
        }
        public Image ModelIMP
        {
            get => Source.ModelIMP;
            set
            {
                if (Source.ModelIMP == value) return;
                Source.ModelIMP = value;
                OnPropertyChanged();
            }
        }
        public Image ModelIR
        {
            get => Source.ModelIR;
            set
            {
                if (Source.ModelIR == value) return;
                Source.ModelIR = value;
                OnPropertyChanged();
            }
        }
        public Image ModelUV
        {
            get => Source.ModelUV;
            set
            {
                if (Source.ModelUV == value) return;
                Source.ModelUV = value;
                OnPropertyChanged();
            }
        }
        public Image ModelVIS
        {
            get => Source.ModelVIS;
            set
            {
                if (Source.ModelVIS == value) return;
                Source.ModelVIS = value;
                OnPropertyChanged();
            }
        }
        public string TrainingStatus
        {
            get => Source.TrainingStatus;
            set
            {
                if (Source.TrainingStatus == value) return;
                Source.TrainingStatus = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
