﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CDDMobile.Database.Models;

namespace CDDMobile.Database.ViewModels
{
    public class TrainingViewModel : INotifyPropertyChanged
    {
        public Training Source { get; private set; }


        public TrainingViewModel() 
        {
            Source = new Training();
        }

        public TrainingViewModel(Training source)
        {
            if (source == null) throw new ArgumentNullException();

            Source = source;
        }

        public string BatchNumber
        {
            get => Source.BatchNumber;
            set
            {
                if (Source.BatchNumber == value) return;
                Source.BatchNumber = value;
                OnPropertyChanged();
            }
        }
        public string Brandname
        {
            get => Source.Brandname;
            set
            {
                if (Source.Brandname == value) return;
                Source.Brandname = value; 
                OnPropertyChanged();
            }
        }
        public string DosageAmount
        {
            get => Source.DosageAmount;
            set
            {
                if (Source.DosageAmount == value) return;
                Source.DosageAmount = value; 
                OnPropertyChanged();
            }
        }
        public string DrugType
        {
            get => Source.DrugType;
            set
            {
                if (Source.DrugType == value) return;
                Source.DrugType = value; 
                OnPropertyChanged();
            }
        }
        public string DrugId
        {
            get => Source.DrugId;
            set
            {
                if (Source.DrugId == value) return;
                Source.DrugId = value; 
                OnPropertyChanged();
            }
        }
        public string GenericName
        {
            get => Source.GenericName;
            set
            {
                if (Source.GenericName == value) return;
                Source.GenericName = value; 
                OnPropertyChanged();
            }
        }
        public string ManufacturerName
        {
            get => Source.ManufacturerName;
            set
            {
                if (Source.ManufacturerName == value) return;
                Source.ManufacturerName = value; 
                OnPropertyChanged();
            }
        }
        public string TrainingAlgorithm
        {
            get => Source.TrainingAlgorithm;
            set
            {
                if (Source.TrainingAlgorithm == value) return;
                Source.TrainingAlgorithm = value; 
                OnPropertyChanged();
            }
        }
        public string TrainingStatus
        {
            get => Source.TrainingStatus;
            set
            {
                if (Source.TrainingStatus == value) return;
                Source.TrainingStatus = value; 
                OnPropertyChanged();
            }
        }
        public string TrainingType
        {
            get => Source.TrainingType;
            set
            {
                if (Source.TrainingType == value) return;
                Source.TrainingType = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
