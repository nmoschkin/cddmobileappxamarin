﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CDDMobile.Database.Models;
using CDDMobile.ViewModels;

namespace CDDMobile.Database.ViewModels
{
    public class DrugViewModel : INotifyPropertyChanged
    {

        public Drug Source { get; private set; }

        public DrugViewModel()
        {
            Source = new Drug();
        }

        public DrugViewModel(Drug source)
        {
            if (source == null) throw new ArgumentNullException();
            Source = source;
        }

        [ResourceReference("BatchNumber")]
        public string BatchNumber
        {
            get => Source.BatchNumber;
            set
            {
                if (Source.BatchNumber == value) return;
                Source.BatchNumber = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("BrandName")]
        public string BrandName
        {
            get => Source.BrandName;
            set
            {
                if (Source.BrandName == value) return;
                Source.BrandName = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("DeploymentStatus")]
        public string DeploymentStatus
        {
            get => Source.DeploymentStatus;
            set
            {
                if (Source.DeploymentStatus == value) return;
                Source.DeploymentStatus = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("DosageAmount")]
        public string DosageAmount
        {
            get => Source.DosageAmount;
            set
            {
                if (Source.DosageAmount == value) return;
                Source.DosageAmount = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("DrugType")]
        public string DrugType
        {
            get => Source.DrugType;
            set
            {
                if (Source.DrugType == value) return;
                Source.DrugType = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("DrugId")]
        public string DrugId
        {
            get => Source.DrugId;
            set
            {
                if (Source.DrugId == value) return;
                Source.DrugId = value;
                OnPropertyChanged();
            }
        }

        [ResourceReference("ExpirationDate")]
        public DateTime ExpirationDate
        {
            get => Source.ExpirationDate;
            set
            {
                if (Source.ExpirationDate == value) return;
                Source.ExpirationDate = value;
                OnPropertyChanged();
            }
        }

        [ResourceReference("GenericName")]
        public string GenericName
        {
            get => Source.GenericName;
            set
            {
                if (Source.GenericName == value) return;
                Source.GenericName = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("ImageUrl")]
        public string ImageUrl
        {
            get => Source.ImageUrl;
            set
            {
                if (Source.ImageUrl == value) return;
                Source.ImageUrl = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("ManufactureDate")]
        public DateTime ManufactureDate
        {
            get => Source.ManufactureDate;
            set
            {
                if (Source.ManufactureDate == value) return;
                Source.ManufactureDate = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("Manufacturer")]
        public string Manufacturer
        {
            get => Source.Manufacturer;
            set
            {
                if (Source.Manufacturer == value) return;
                Source.Manufacturer = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("TrainingAlgorithm")]
        public string TrainingAlgorithm
        {
            get => Source.TrainingAlgorithm;
            set
            {
                if (Source.TrainingAlgorithm == value) return;
                Source.TrainingAlgorithm = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("TrainingStatus")]
        public string TrainingStatus
        {
            get => Source.TrainingStatus;
            set
            {
                if (Source.TrainingStatus == value) return;
                Source.TrainingStatus = value;
                OnPropertyChanged();
            }
        }


        [ResourceReference("TrainingType")]
        public TrainingType TrainingType
        {
            get => Source.TrainingType;
            set
            {
                if (Source.TrainingType == value) return;
                Source.TrainingType = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
