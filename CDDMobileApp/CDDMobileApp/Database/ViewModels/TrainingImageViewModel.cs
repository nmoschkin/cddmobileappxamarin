﻿using CDDMobile.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using CDDMobile.ViewModels;
using CDDMobile.Network;

namespace CDDMobile.Database.ViewModels
{
    public class TrainingImageViewModel : ViewModelBase
    {

        private BTResult source;

        public TrainingImageViewModel(BTResult source)
        {
            this.Source = source;
        }

        public BTResult Source
        {
            get => source;
            set
            {
                source = value;
                OnPropertyChanged();
            }
        }

        public CommandResult Result
        {
            get => Source.Result;
            set
            {
                Source.Result = value;
                OnPropertyChanged();
            }
        }

        public ImageSource Visible
        {
            get => Source.Visible;
            set
            {
                Source.Visible = value;
                OnPropertyChanged();
            }
        }


        public ImageSource Infrared
        {
            get => Source.Infrared;
            set
            {
                Source.Infrared = value;
                OnPropertyChanged();
            }
        }

        public ImageSource Ultraviolet
        {
            get => Source.Ultraviolet;
            set
            {
                Source.Ultraviolet= value;
                OnPropertyChanged();
            }
        }


        public ImageSource Imprint
        {
            get => Source.Imprint;
            set
            {
                Source.Imprint = value;
                OnPropertyChanged();
            }
        }

    }
}
