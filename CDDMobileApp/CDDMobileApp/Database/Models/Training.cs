﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Database.Models
{
    public class Training
    {

        [JsonProperty("batchNumber")]
        public string BatchNumber { get; set; }

        [JsonProperty("brandname")]
        public string Brandname { get; set; }

        [JsonProperty("dosageAmount")]
        public string DosageAmount { get; set; }

        [JsonProperty("drugType")]
        public string DrugType { get; set; }

        [JsonProperty("drugid")]
        public string DrugId { get; set; }

        [JsonProperty("genericname")]
        public string GenericName { get; set; }

        [JsonProperty("manuName")]
        public string ManufacturerName { get; set; }

        [JsonProperty("trainingAlgorithm")]
        public string TrainingAlgorithm { get; set; }

        [JsonProperty("trainingStatus")]
        public string TrainingStatus { get; set; }

        [JsonProperty("trainingType")]
        public string TrainingType { get; set; }

        public override string ToString()
        {
            return GenericName;
        }

    }

}
