﻿using CDDMobile.Database.ViewModels;
using CDDMobile.ViewModels;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Text;
using Telerik.XamarinForms.Input.Calendar.Commands;
using Xamarin.Forms;
using System.Windows.Input;

namespace CDDMobile.Database.Models
{
    public class DeploymentBucket : ViewModelBase
    {
        public delegate void ClickedEvent(object sender, EventArgs e);
        public event ClickedEvent Clicked;
        
        private DrugViewModel drug;

        private Command clickedCommand; 

        public ICommand ClickedCommand
        {
            get => (ICommand)clickedCommand;
        }

        public DeploymentBucket()
        {
            clickedCommand = new Command(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Clicked?.Invoke(this, new EventArgs());
                });
            });
        }

        public DrugViewModel Drug
        {
            get => drug;
            set
            {
                if (drug == value) return;
                drug = value;
                OnPropertyChanged();
            }
        }

        private string drugId;

        public string DrugId
        { get => drugId;
            set
            {
                if (drugId == value) return;
                drugId = value;

                OnPropertyChanged();
            }
        }

        private DateTime timeStamp;

        public DateTime Timestamp
        {
            get => timeStamp;
            set
            {
                if (timeStamp == value) return;
                timeStamp = value;

                OnPropertyChanged();
            }
        }

        private string[] files;

        public string[] Files
        {
            get => files;
            set
            {
                files = value;
                OnPropertyChanged();
            }
        }


        private Stream[] fileStreams;

        public Stream[] FileStreams
        {
            get => fileStreams;
            set
            {
                fileStreams = value;
                OnPropertyChanged();
            }
        }

    }
}
