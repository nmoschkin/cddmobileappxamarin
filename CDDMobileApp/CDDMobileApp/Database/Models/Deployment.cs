﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Database.Models
{

    public class Deployment
    {

        [JsonProperty("brandname")]
        public string BrandName { get; set; }

        [JsonProperty("deploymentStatus")]
        public string DeploymentStatus { get; set; }

        [JsonProperty("drugid")]
        public string DrugId { get; set; }

        [JsonProperty("genericname")]
        public string GenericName { get; set; }

        [JsonProperty("modelIMP")]
        public Image ModelIMP { get; set; }

        [JsonProperty("modelIR")]
        public Image ModelIR { get; set; }

        [JsonProperty("modelUV")]
        public Image ModelUV { get; set; }

        [JsonProperty("modelVIS")]
        public Image ModelVIS { get; set; }

        [JsonProperty("trainingStatus")]
        public string TrainingStatus { get; set; }

    }


}
