﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace CDDMobile.Database.Models
{

    public class BucketList
    {

        [JsonProperty("prefixes")]
        public List<string> Prefixes { get; set; }
    
        [JsonProperty("items")]
        public List<BucketItem> Items { get; set; }

        [JsonProperty("nextPageToken")]
        public string NextPageToken { get; set; }

    }

    public class BucketItem
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("bucket")]
        public string Bucket { get; set; }

        public string GetFilename()
        {
            var i = Name.LastIndexOf("/");
            return Name.Substring(i + 1);
        }

        public string GetDrug()
        {
            var i = Name.IndexOf("/");
            var s = Name.Substring(0, i);

            return s.ToLower();
        }

    }
}
