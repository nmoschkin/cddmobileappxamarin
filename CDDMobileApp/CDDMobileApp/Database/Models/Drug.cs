﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;


namespace CDDMobile.Database.Models
{
    public class Drug
    {

        [JsonProperty("batchno")]
        public string BatchNumber { get; set; }


        [JsonProperty("brandname")]
        public string BrandName { get; set; }


        [JsonProperty("deploymentStatus")]
        public string DeploymentStatus { get; set; }


        [JsonProperty("dosageamt")]
        public string DosageAmount { get; set; }


        [JsonProperty("drugType")]
        public string DrugType { get; set; }


        [JsonProperty("drugid")]
        public string DrugId { get; set; }


        [JsonProperty("expirydate")]
        public DateTime ExpirationDate { get; set; }


        [JsonProperty("genericname")]
        public string GenericName { get; set; }


        [JsonProperty("imageurl")]
        public string ImageUrl { get; set; }


        [JsonProperty("manudate")]
        public DateTime ManufactureDate { get; set; }


        [JsonProperty("manuname")]
        public string Manufacturer { get; set; }


        [JsonProperty("trainingAlgorithm")]
        public string TrainingAlgorithm { get; set; }


        [JsonProperty("trainingStatus")]
        public string TrainingStatus { get; set; }


        [JsonProperty("trainingType")]
        public TrainingType TrainingType { get; set; }



    }

}
