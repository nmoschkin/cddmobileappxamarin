﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using CDDMobile.Localization;
using CDDMobile.ViewModels;
using Newtonsoft.Json;

namespace CDDMobile.Database
{
    [JsonConverter(typeof(TrainingTypeJsonConverter))]
    public enum TrainingType
    {
        
        [ResourceReference("None")]
        None = 0,

        [ResourceReference("Authentic")]
        Authentic = 1,


        [ResourceReference("Counterfeit")]
        Counterfeit = 2
    }


    public class TrainingTypeJsonConverter : JsonConverter<TrainingType>
    {
        public override TrainingType ReadJson(JsonReader reader, Type objectType, [AllowNull] TrainingType existingValue, bool hasExistingValue, JsonSerializer serializer)
        {

            string value = (string)reader.Value ?? "";

            switch (value.ToLower())
            {
                case "authentic":
                    return TrainingType.Authentic;

                case "counterfeit":
                    return TrainingType.Counterfeit;

                default:
                    return TrainingType.None;
            }
            

        }

        public override void WriteJson(JsonWriter writer, [AllowNull] TrainingType value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }
    }


}
