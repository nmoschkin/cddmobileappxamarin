﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CDDMobile.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationBar : ContentView
    {

        public delegate void BackButtonClickedEvent(object sender, EventArgs e);
        public event BackButtonClickedEvent BackButtonClicked;

        public delegate void MenuClickedEvent(object sender, EventArgs e);
        public event MenuClickedEvent MenuClicked;

        private readonly double DefaultHeight = 48;
        private readonly double DefaultWidth = 96;
        private readonly double DefaultFontSize = 20;
        private readonly double DefaultLabelSize = 14;

        private double InitFontRatio;
        private double InitLabelRatio;
        private double InitRatio;

        public NavigationBar()
        {
            InitializeComponent();
            
            InitLabelRatio = DefaultLabelSize / DefaultHeight;
            InitFontRatio = DefaultFontSize / DefaultHeight;
            InitRatio = DefaultWidth / DefaultHeight;
        }

        protected override void OnSizeAllocated(double width, double height)
        {

            if (height <= 0 || width <= 0) return;
            
            ContentGrid.HeightRequest = height;
            ContentGrid.WidthRequest = width;

            NearColumn.Width = FarColumn.Width = (height * InitRatio);

            double calcFontSize = height * InitFontRatio;
            double calcLabelSize = height * InitLabelRatio;

            lblBackArrow.WidthRequest = (height / InitRatio);
            lblMenuIcon.WidthRequest = (height / InitRatio);

            lblBackArrow.HeightRequest = height;
            lblMenuIcon.HeightRequest = height;

            lblBackArrow.FontSize = calcFontSize;
            lblMenuIcon.FontSize = calcFontSize;

            
            lblTitle.FontSize = calcLabelSize;

            base.OnSizeAllocated(width, height);
        }

        public static BindableProperty IsMenuVisibleProperty =
            BindableProperty.Create(
                "IsMenuVisible",
                typeof(bool),
                typeof(NavigationBar),
                false,
                BindingMode.TwoWay, null, OnIsMenuVisibleChanged);

        static void OnIsMenuVisibleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is NavigationBar inst)
            {
                inst.lblMenuIcon.IsVisible = (bool)newValue;
            }

        }

        public bool IsMenuVisible
        {
            get
            {
                return (bool)GetValue(IsMenuVisibleProperty);
            }
            set
            {
                SetValue(IsMenuVisibleProperty, value);
            }
        }


        public static BindableProperty IsBackArrowVisibleProperty =
            BindableProperty.Create(
                "IsBackArrowVisible",
                typeof(bool),
                typeof(NavigationBar),
                false,
                BindingMode.TwoWay, null, OnIsBackArrowVisibleChanged);

        static void OnIsBackArrowVisibleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is NavigationBar inst)
            {
                inst.lblBackArrow.IsVisible = (bool)newValue;
            }
        }

        public bool IsBackArrowVisible
        {
            get
            {
                return (bool)GetValue(IsBackArrowVisibleProperty);
            }
            set
            {
                SetValue(IsBackArrowVisibleProperty, value);
            }
        }


        public static BindableProperty TitleProperty = 
            BindableProperty.Create(
                "Title", 
                typeof(string), 
                typeof(NavigationBar), 
                "",
                BindingMode.TwoWay, null, null, OnTitleChanged);

        static void OnTitleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is NavigationBar inst)
            {
                inst.lblTitle.Text = (string)newValue;
            }
        }

        public string Title
        {
            get
            {
                return (string)GetValue(TitleProperty);
            }
            set
            {
                SetValue(TitleProperty, value);
            }
        }

        private void BackButton_Tapped(object sender, EventArgs e)
        {
            BackButtonClicked?.Invoke(this, new EventArgs());
        }

        private void MenuButton_Tapped(object sender, EventArgs e)
        {
            MenuClicked?.Invoke(this, new EventArgs());
        }


    }
}