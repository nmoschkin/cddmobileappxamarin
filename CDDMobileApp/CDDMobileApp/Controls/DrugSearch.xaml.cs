﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Telerik.XamarinForms.Input;

using CDDMobile.Filters;
using CDDMobile.Localization.Resources;
using CDDMobile.ViewModels;
using Telerik.XamarinForms.Input.AutoComplete;
using System.ComponentModel;
using System.Diagnostics;
using CDDMobile.Database.ViewModels;

namespace CDDMobile.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DrugSearch : ContentView
    {
        // private string currentText;
        // private bool isRemoteSearchRunning;

        //private DrugSearchViewModel vm;


        //public DrugSearch(DrugSearchViewModel vm) : this()
        //{
        //    this.ViewModel = vm;
            
        //}

        public DrugSearch()
        {
            InitializeComponent();
            DrugSearchBox.SuggestionItemSelected += DrugSearchBox_SuggestionItemSelected;
        }

        //internal DrugSearchViewModel ViewModel
        //{
        //    get => vm;
        //    set
        //    {
        //        vm = value;
        //        BindingContext = vm;
        //        OnPropertyChanged();
        //    }
        //}

        private void DrugSearchBox_SuggestionItemSelected(object sender, SuggestionItemSelectedEventArgs e)
        {
            SelectedItem = e.DataItem;
            //DrugSearchBox.Text = (e.DataItem as DrugViewModel).BrandName;
        }

        public static readonly BindableProperty SelectedItemProperty = 
            BindableProperty.Create(
                "SelectedItem", 
                typeof(object), 
                typeof(DrugSearch), 
                null, 
                BindingMode.TwoWay, 
                null, 
                OnSelectedItemChanged); 

        public object SelectedItem
        {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        static void OnSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {

        }


        private void DrugSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            //if (SelectedItem is DrugViewModel d)
            //{
            //    if (d.BrandName == e.NewTextValue) return;
            //}

            //SelectedItem = e.NewTextValue;

            //var autoCompleteView = (RadAutoCompleteView)sender;
            //autoCompleteView.ItemsSource = null;

            //this.currentText = e.NewTextValue ?? "";

            //if (this.currentText.Length >= autoCompleteView.SearchThreshold && !this.isRemoteSearchRunning)
            //{
            //    this.isRemoteSearchRunning = true;



            //    Device.StartTimer(TimeSpan.FromMilliseconds(1500), () =>
            //    {
            //        this.isRemoteSearchRunning = false;
            //        string searchText = this.currentText.ToLower();
            //        // autoCompleteView.ItemsSource = this.viewModel.Source.Where(i => i.Name.ToLower().Contains(searchText));




            //        return false;
            //    });
            //}
        }
    }
}