﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Telerik.XamarinForms.Input;

using System.ComponentModel;
using System.Collections.Specialized;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices;
using Telerik.XamarinForms.Primitives.CheckBox.Commands;

namespace CDDMobile.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ButtonView : ContentView
    {

        public delegate void ButtonClickedEvent(object sender, ButtonClickedEventArgs e);
        public event ButtonClickedEvent ButtonClicked;

        bool suspendChanges = false;
        bool sited = false;

        public ButtonView()
        {
            InitializeComponent();
           
            Buttons = new ObservableCollection<RadButton>();
        }

        public static BindableProperty ButtonsProperty =
             BindableProperty.Create(
                 "Buttons",
                 typeof(ObservableCollection<RadButton>),
                 typeof(ButtonView),
                 null,
                 BindingMode.TwoWay, null, OnButtonsChanged);

        static void OnButtonsChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is ButtonView inst)
            {

                if (oldValue != null && oldValue is ObservableCollection<RadButton> bOld)
                {
                    bOld.CollectionChanged -= inst.ButtonsChanged;
                }

                inst.RenderGrid();

                if (newValue != null && newValue is ObservableCollection<RadButton> bNew)
                {
                    bNew.CollectionChanged += inst.ButtonsChanged;
                }

            }
        }

        private void internalButtonClicked(object sender, EventArgs e)
        {
            int i = 0;
            foreach (object child in ContentGrid.Children)
            {
                if (sender == child && child is RadButton b)
                {
                    ButtonClicked?.Invoke(sender, new ButtonClickedEventArgs(i, b.Text));
                    break;
                }
                i++;
            }
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
        }

        protected override void OnParentSet()
        {
            base.OnParentSet();
            sited = true;

            RenderGrid();
        }

        public RadButton AddButton(string text)
        {
            RadButton button = new RadButton();
            button.Text = text;

            Buttons.Add(button);
            return button;
        }

        public List<RadButton> AddButtonRange(IEnumerable<string> strings)
        {

            RadButton button;
            List<RadButton> buttons = new List<RadButton>();

            suspendChanges = true;

            foreach (var s in strings)
            {
                button = new RadButton();

                button.Text = s;
                buttons.Add(button);
                Buttons.Add(button);
            }

            suspendChanges = false;
            RenderGrid();

            return buttons;
        }

        private void RenderGrid()
        {
            if (suspendChanges || !sited) return;

            foreach (object child in ContentGrid.Children)
            {
                if (child is RadButton b)
                {
                    b.Clicked -= internalButtonClicked;
                }
            }

            ContentGrid.Children.Clear();
            ContentGrid.RowDefinitions.Clear();
            ContentGrid.ColumnDefinitions.Clear();

            ContentGrid.ColumnDefinitions.Add(new ColumnDefinition());
            
            int i;

            for (i = 0; i < Buttons.Count; i++)
            {
                ContentGrid.RowDefinitions.Add(new RowDefinition());
            }

            i = 0;

            foreach (var b in Buttons) 
            {
                FormatButton(b);

                b.SetValue(Grid.RowProperty, i++);
                ContentGrid.Children.Add(b);
                b.Clicked += internalButtonClicked;
            }

        }

        private void FormatButton(RadButton button)
        {
            button.Margin = new Thickness(48, 0);
            button.HorizontalOptions = LayoutOptions.Fill;
            button.VerticalOptions = LayoutOptions.Center;
        }


        private void ButtonsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RenderGrid();
        }

        public ObservableCollection<RadButton> Buttons
        {
            get
            {
                return (ObservableCollection<RadButton>)GetValue(ButtonsProperty);
            }
            set
            {
                SetValue(ButtonsProperty, value);
            }
        }
    }


}