﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Xamarin.Forms;

namespace CDDMobile.Controls
{
    public class ButtonClickedEventArgs : EventArgs
    {
        public string ButtonText { get; private set; }

        public int ButtonIndex { get; private set; }


        public ButtonClickedEventArgs(int index, string text)
        {
            ButtonText = text;
            ButtonIndex = index;
        }



    }
}
