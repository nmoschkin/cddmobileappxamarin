﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace CDDMobile.Network
{
    public class NetSocket : ISocketStream
    {

        private Socket socket;

        public NetSocket(Socket socket)
        {
            this.socket = socket;
        }

        public void Close()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

        public void Dispose() => socket.Dispose();

        public int ReceiveData(byte[] buffer, int offset, int count) 
        {
            return socket.Receive(buffer, offset, count, SocketFlags.None);
        }

        public void SendData(byte[] buffer)
        {
            socket.Send(buffer);
        }

        public void SendData(byte[] buffer, int offset, int count)
        {
            socket.Send(buffer, offset, count, SocketFlags.None);
        }
    }
}
