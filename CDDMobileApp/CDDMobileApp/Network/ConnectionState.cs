﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace CDDMobile.Network
{
    
    public enum ConnectionState
    {
        Disconnected,
        Connecting,
        Connected, 
        Receiving,
        Transmitting,
        Done,
        Error,
        Cancel
    }


}
