﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace CDDMobile.Network
{
   
    public interface IBTSerial
    {

        string DeviceName { get; set; }

        IList<BTDevice> GetDeviceList(int btclass = 0x180620);

        Task<ISocketStream> GetSocket();

        bool HasAdapter { get; }

        bool AdapterEnabled { get; }
        
        bool EnableAdapter();

        void OpenBluetoothSettings();

    }



}
