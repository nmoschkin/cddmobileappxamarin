﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using CDDMobile.Database.Models;

namespace CDDMobile.Network
{

    public enum TestResult : byte
    {
        Counterfeit,
        Authentic
    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]    
    public struct MODEL_DATA_STRUCT
    {
        static int structLen;

        public const byte CMD_IDENTIFY = 0;
        public const byte CMD_CAPTURE = 1;
        public const byte CMD_SEND_DATAMODEL = 0x80;
        public const byte CMD_NEGOTIATE = 0x81;

        public const byte WITH_VIS = 2;
        public const byte WITH_UV = 4;
        public const byte WITH_IR = 8;
        public const byte WITH_PROC = 16;

        public const int STRUCT_LEN = 82;

        public const int VERSION_CONST = 2;

        static MODEL_DATA_STRUCT()
        {
            structLen = Marshal.SizeOf<MODEL_DATA_STRUCT>();
        }

        // length of data, including size, itself.
        public int size;

        // function
        public byte command;

        // result of function (0 or 1)
        public TestResult result;

        // size of visible light image
        public int vis_size;

        // size of UV image
        public int uv_size;

        // size of IR image
        public int ir_size;

        // proc image size
        public int imp_size;

        // Was reserved, as of version 2 it is version.
        public int version;

        public int reserved2;

        // *** VERSION 2 FIELDS - BEGIN

        // FP result value
        public TestResult vis_result;
        public double vis_pct;

        // FP result value
        public TestResult uv_result;
        public double uv_pct;

        // FP result value
        public TestResult ir_result;
        public double ir_pct;

        // FP result value
        public TestResult imp_result;
        public double imp_pct;

        public int reserved3;
        public int reserved4;
        public int reserved5;
        public int reserved6;

        // *** VERSION 2 FIELDS - END

        // data payload / binary data
        public byte[] data;

        public static byte[] FormatModelFilePackage(string drugId, ImageType type, byte[] fileData)
        {
            string[] its = new string[] { "", "vis", "uv", "ir", "imp" };
            string it = its[(int)type];

            drugId = drugId.Trim().Replace("\x0", "");

            byte[] drugUtf8 = System.Text.UTF8Encoding.UTF8.GetBytes(drugId);
            byte[] typeUtf8 = System.Text.UTF8Encoding.UTF8.GetBytes(it);

            List<byte> output = new List<byte>();

            output.AddRange(drugUtf8);
            output.Add(0);
            output.AddRange(typeUtf8);
            output.Add(0);
            output.AddRange(fileData);

            return output.ToArray();
        }

        /// <summary>
        /// Sends the structure (but not the data)
        /// </summary>
        /// <param name="stream"></param>
        public void SendStruct(ISocketStream stream)
        {
            byte[] buffer;

            if (data != null && data.Length > 0)
            {
                size = STRUCT_LEN + data.Length;
            }
            else
            {
                size = STRUCT_LEN;
            }

            buffer = BitConverter.GetBytes(size);
            Array.Resize(ref buffer, 22);

            buffer[4] = command;
            stream.SendData(buffer);

            version = VERSION_CONST;

            buffer = BitConverter.GetBytes(version);
            Array.Resize(ref buffer, 8);
            stream.SendData(buffer);

            buffer = new byte[52];
            stream.SendData(buffer);

            return;
        }

        /// <summary>
        /// Receives the structure (but not the data)
        /// </summary>
        /// <param name="stream">The stream to receive on.</param>
        /// <returns></returns>
        public static MODEL_DATA_STRUCT ReceiveStruct(ISocketStream stream)
        {
            var data = new MODEL_DATA_STRUCT();
            byte[] buffer = new byte[9];

            stream.ReceiveData(buffer, 0, 4);
            data.size = BitConverter.ToInt32(buffer);

            stream.ReceiveData(buffer, 0, 1);
            data.command = buffer[0];

            stream.ReceiveData(buffer, 0, 1);
            data.result = (TestResult)buffer[0];

            stream.ReceiveData(buffer, 0, 4);
            data.vis_size = BitConverter.ToInt32(buffer);

            stream.ReceiveData(buffer, 0, 4);

            data.uv_size = BitConverter.ToInt32(buffer);
            stream.ReceiveData(buffer, 0, 4);

            data.ir_size = BitConverter.ToInt32(buffer);

            stream.ReceiveData(buffer, 0, 4);
            data.imp_size = BitConverter.ToInt32(buffer);

            stream.ReceiveData(buffer, 0, 4);
            data.version = BitConverter.ToInt32(buffer);

            // reserved          
            stream.ReceiveData(buffer, 0, 4);
            data.reserved2 = BitConverter.ToInt32(buffer);

            if (data.version >= 2)
            {
                stream.ReceiveData(buffer, 0, 9);
                data.vis_result = (TestResult)buffer[0];
                data.vis_pct = BitConverter.ToDouble(buffer, 1);

                stream.ReceiveData(buffer, 0, 9);
                data.uv_result = (TestResult)buffer[0];
                data.uv_pct = BitConverter.ToDouble(buffer, 1);

                stream.ReceiveData(buffer, 0, 9);
                data.ir_result = (TestResult)buffer[0];
                data.ir_pct = BitConverter.ToDouble(buffer, 1);

                stream.ReceiveData(buffer, 0, 9);
                data.imp_result = (TestResult)buffer[0];
                data.imp_pct = BitConverter.ToDouble(buffer, 1);


                // reserved          
                stream.ReceiveData(buffer, 0, 4);
                data.reserved3 = BitConverter.ToInt32(buffer);

                // reserved          
                stream.ReceiveData(buffer, 0, 4);
                data.reserved4 = BitConverter.ToInt32(buffer);

                // reserved          
                stream.ReceiveData(buffer, 0, 4);
                data.reserved5 = BitConverter.ToInt32(buffer);

                // reserved          
                stream.ReceiveData(buffer, 0, 4);
                data.reserved6 = BitConverter.ToInt32(buffer);


            }

            //int len = data.size = STRUCT_LEN - 62;

            //if (len > 0)
            //{
            //    data.data = new byte[len];
            //    stream.ReceiveData(data.data, 0, len);
            //}

            return data;

        }


    }

}
