﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace CDDMobile.Network
{
    public interface ISocketStream
    {

        void SendData(byte[] buffer);
    
        void SendData(byte[] buffer, int offset, int count);

        int ReceiveData(byte[] buffer, int offset, int count);

        void Close();

        void Dispose();

    }


}
