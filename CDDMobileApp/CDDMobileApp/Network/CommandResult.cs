﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Network
{
    public enum CommandResult
    {
        CaptureOnly,
        Authentic,
        Counterfeit
    }
}
