﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Network
{
    public class BTDevice
    {
                
        public string Address { get; private set; }

        public string Name { get; private set; }

        public int Class { get; private set; }

        public string ClassString { get; private set; }

        public BTDevice(string addr, string name, int cls)
        {

            Class = cls;
            ClassString = cls.ToString("x");
            Address = addr;
            Name = name;
        }

        public BTDevice(string addr, string name, string cls) 
        {
            ClassString = cls;
            Class = 0;
            Address = addr;
            Name = name;
        }

    }
}
