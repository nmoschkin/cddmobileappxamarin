﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Network
{
    public class BluetoothUnavailableException : System.Exception
    {

        public BluetoothUnavailableException(string message) : base(message)
        {
            
        }

        public BluetoothUnavailableException() : base()
        {

        }

    }
}
