﻿using System;
using System.Collections.Generic;
using System.Text;
using CDDMobile.Database;
using CDDMobile.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.IO;
using Acr.UserDialogs.Forms;
using CDDMobile.Localization.Resources;
using CDDMobile.Database.Models;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using DataTools.Text;
using System.Net;
using Telerik.XamarinForms.DataGrid;

namespace CDDMobile.Network
{
    public class BluetoothConnection
    {
        readonly IBTSerial bt;

        public delegate void NotifyProgressEvent(object sender, ProgressEventArgs e);

        public event NotifyProgressEvent NotifyProgress;


        public void OpenSettings()
        {
            bt.OpenBluetoothSettings();
        }

        public static bool CheckAdapterState()
        {
            var bt = DependencyService.Get<IBTSerial>();
            if (bt.HasAdapter)
            {
                if (bt.AdapterEnabled)
                {
                    return true;
                }
            }

            return false;
        }


        public static async Task<bool> CheckAdapterStateAsync(bool askEnable = false)
        {
            var bt = DependencyService.Get<IBTSerial>();
            if (bt.HasAdapter)
            {
                if (bt.AdapterEnabled)
                {
                    return true;
                }
                else
                {
                    if (askEnable)
                    {
                        var cfg = new ConfirmConfig()
                        {
                            CancelLabel = AppResources.No,
                            OkLabel = AppResources.Yes,
                            Title = AppResources.Bluetooth,
                            Message = AppResources.AskActivateBluetooth
                        };

                        var dlg = new UserDialogs();

                        var result = await dlg.Confirm(cfg);

                        if (result)
                        {
                            result = bt.EnableAdapter();

                            if (result)
                            {
                                cfg.Message = AppResources.BluetoothActivated;

                                await dlg.Alert(cfg);
                                return true;
                            }
                            else
                            {
                                cfg.Message = AppResources.ErrorActivateBluetooth;
                                await dlg.Alert(cfg);
                            }
                        }
                    }
                }
            }

            return false;
        }

        public BluetoothConnection()
        {
            bt = DependencyService.Get<IBTSerial>();
            DeviceName = Settings.DeviceName;    
        }

        public BluetoothConnection(string deviceName, bool updateSettings = true)
        {
            bt = DependencyService.Get<IBTSerial>();
            DeviceName = deviceName;

            if (updateSettings) Settings.DeviceName = deviceName;
        }

        public string DeviceName
        {
            get => bt.DeviceName;
            set => bt.DeviceName = value;
        }

        public static IList<BTDevice> GetPairedDeviceList()
        {
            var bt = DependencyService.Get<IBTSerial>();
            return bt.GetDeviceList();
        }

        public static List<string> GetPairedDeviceNames()
        {
            var i = GetPairedDeviceList();
            var o = new List<string>();

            foreach(var d in i)
            {
                o.Add(d.Name);
            }

            return o;
        }

        public async Task<bool> TransmitModel(string drugId, ImageType type, Stream data, CancellationToken? cancel = null)
        {
            
            byte[] bytes = new byte[data.Length];
            
            await data.ReadAsync(bytes, 0, (int)data.Length);

            var fdat = MODEL_DATA_STRUCT.FormatModelFilePackage(drugId, type, bytes);
            var result = await SendCommand(MODEL_DATA_STRUCT.CMD_SEND_DATAMODEL, fdat, cancel);

            return (result.result == TestResult.Authentic);

        }

        public async Task<BTResult> Capture(bool detect = false, string drugId = null, CancellationToken? cancel = null)
        {
            if (detect && string.IsNullOrEmpty(drugId)) throw new ArgumentNullException("Must specify drugId if detect is set to 'true'");

            if (bt.DeviceName == null) return null;

            byte cmd = detect ? MODEL_DATA_STRUCT.CMD_IDENTIFY : MODEL_DATA_STRUCT.CMD_CAPTURE;
            
            if (Settings.ShowVisible) cmd |= 2;
            if (Settings.ShowUV) cmd |= 4;
            if (Settings.ShowIR) cmd |= 8;
            if (Settings.ShowImprint) cmd |= 16;

            MODEL_DATA_STRUCT ret = await SendCommand(cmd, drugId?.ToLower() ?? null, cancel);


            if (ret.data != null && ret.data.Length != 0)
            {

                try
                {
                    var response = new BTResult();

                    Stream vis, uv, ir, imp;
                    vis = uv = ir = imp = null;

                    SplitImages(ret, ref vis, ref uv, ref ir, ref imp);


                    // make copies of the stream, for later.

                    response.IrStream = new MemoryStream();
                    response.UvStream = new MemoryStream();
                    response.VisStream = new MemoryStream();
                    response.ImpStream = new MemoryStream();

                    vis.CopyTo(response.VisStream);
                    uv.CopyTo(response.UvStream);
                    ir.CopyTo(response.IrStream);
                    imp.CopyTo(response.ImpStream);

                    vis.Seek(0, SeekOrigin.Begin);
                    uv.Seek(0, SeekOrigin.Begin);
                    ir.Seek(0, SeekOrigin.Begin);
                    imp.Seek(0, SeekOrigin.Begin);


                    var assembly = this.GetType().Assembly;

                    if (vis != null)
                    {
                        response.Visible = ImageSource.FromStream(() => vis);
                    }
                    if (uv != null)
                    {
                        response.Ultraviolet = ImageSource.FromStream(() => uv);
                    }
                    if (ir != null)
                    {
                        response.Infrared = ImageSource.FromStream(() => ir);
                    }
                    if (imp != null)
                    {
                        response.Imprint = ImageSource.FromStream(() => imp);
                    }

                    response.Result = detect ? ret.result == TestResult.Authentic ? CommandResult.Authentic : CommandResult.Counterfeit : CommandResult.CaptureOnly;

                    return response;
                }
                catch 
                { 
                
                
                }
            }

            return null;

        }

        private void SplitImages(MODEL_DATA_STRUCT data, ref Stream vis, ref Stream uv, ref Stream ir, ref Stream imp)
        {

            if ((data.command & 2) == 2)
            {
                vis = new MemoryStream();
                vis.WriteAsync(data.data, 0, data.vis_size);
                vis.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                vis = null;
            }

            if ((data.command & 4) == 4)
            {
                uv = new MemoryStream();
                uv.WriteAsync(data.data, data.vis_size, data.uv_size);
                uv.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                uv = null;
            }

            if ((data.command & 8) == 8)
            {
                ir = new MemoryStream();
                ir.WriteAsync(data.data, data.vis_size + data.uv_size, data.ir_size);
                ir.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                ir = null;
            }


            if ((data.command & 16) == 16)
            {
                imp = new MemoryStream();
                imp.WriteAsync(data.data, data.vis_size + data.uv_size + data.ir_size, data.imp_size);
                imp.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                imp = null;
            }

        }

        #region Network Negotiation 


        // switch to WiFi if we can.
        internal async Task<ISocketStream> NegotiateConnection(ISocketStream btsock)
        {

            try
            {
                var ifaces = NetworkInterface.GetAllNetworkInterfaces();

                byte[] signal = BitConverter.GetBytes((int)5);
                Array.Resize(ref signal, 5);

                signal[4] = 0x81;

                btsock.SendData(signal);

                await Task.Yield();
                await Task.Delay(100);

                btsock.ReceiveData(signal, 0, 4);

                int len = BitConverter.ToInt32(signal, 0);
                btsock.ReceiveData(signal, 0, 4);

                int success = BitConverter.ToInt32(signal, 0);
                if (success == 0) return null;

                Array.Resize(ref signal, len - 8);

                btsock.ReceiveData(signal, 0, len - 8);

                string ipaddr = UTF8Encoding.UTF8.GetString(signal);

                int ipintA = TextTools.IPInt(ipaddr);
                int ipintB;
                int nmask;

                IPAddress faddr = IPAddress.None;
                IPAddress raddr = IPAddress.Parse(ipaddr);

                bool bFound = false;

                foreach (var iface in ifaces)
                {
                    var ipp = iface.GetIPProperties();

                    foreach (var taddr in ipp.UnicastAddresses)
                    {

                        ipintB = TextTools.IPInt(taddr.Address.ToString());
                        nmask = TextTools.IPInt(taddr.IPv4Mask.ToString());

                        if (ipintB == 0 || nmask == 0) continue;

                        if ((ipintA & nmask) == (ipintB & nmask))
                        {
                            faddr = taddr.Address;
                            bFound = true;
                            break;
                        }
                    }

                    if (bFound) break;
                }

                if (!bFound)
                {
                    signal = BitConverter.GetBytes((int)0);
                    btsock.SendData(signal);

                    return null;
                }
                else
                {
                    signal = BitConverter.GetBytes((int)1);
                    btsock.SendData(signal);
                }

                await Task.Delay(500);

                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                socket.Bind(new IPEndPoint(faddr, 3270));

                socket.Connect(new IPEndPoint(raddr, 3270));
                
                //Task t = socket.ConnectAsync(raddr, 3270);

                //DateTime tnow = DateTime.Now;
                //DateTime timedOut = tnow.AddSeconds(30);

                //do
                //{
                //    await Task.Yield();
                //    if (socket.Poll(10000, SelectMode.SelectWrite)) break;

                //} while (DateTime.Now < timedOut);

                //if (!socket.Poll(0, SelectMode.SelectWrite))
                //{
                //    socket.Dispose();
                //    return null;
                //}

                return new NetSocket(socket);

            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        #endregion


        #region Data Exchange


        internal async Task<MODEL_DATA_STRUCT> SendCommand(byte command, string input = null, CancellationToken? cancel = null)
        {
            if (input != null)
            {
                return await SendCommand(command, Encoding.UTF8.GetBytes(input), cancel);
            }
            else
            {
                return await SendCommand(command, new byte[] { }, cancel);
            }
        }

        internal async Task<MODEL_DATA_STRUCT> SendCommand(byte command, byte[] input, CancellationToken? cancelToken = null)
        {
            CancellationToken cancel;

            MODEL_DATA_STRUCT data = new MODEL_DATA_STRUCT();
            int size = MODEL_DATA_STRUCT.STRUCT_LEN;
            int rslen;

            byte[] ret;

            if (cancelToken != null)
            {
                cancel = (CancellationToken)cancelToken;
            }
            else
            {
                cancel = new CancellationToken();
            }

            data.command = command;

            if (input != null && input.Length > 0)
            {
                data.data = input;
                size += data.data.Length;
            }

            NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Connecting));
            await Task.Yield();

            var _socket = await bt.GetSocket();

            if (((CancellationToken)cancel).IsCancellationRequested)
            {
                _socket.Close();
                return new MODEL_DATA_STRUCT();
            }

            NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Connected));
            await Task.Yield();

            if (_socket != null)
            {
                try
                {

                    ISocketStream ipsock = null;

                    NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Transmitting));
                    await Task.Yield();


                    if ((data.data?.Length ?? 0) > 10240 && Settings.UseWiFi)
                    {
                        NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Connecting, AppResources.TryingWifi));
                        await Task.Yield();

                        ipsock = await NegotiateConnection(_socket);

                        NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Connected));
                        await Task.Yield();
                    }

                    if (ipsock != null)
                    {
                        _socket.Close();
                        _socket.Dispose();

                        _socket = ipsock;
                    }

                    data.SendStruct(_socket);

                    if (data.data != null)
                    {
                        if (data.data.Length > 10240)
                        {
                            int tLen = data.data.Length;
                            int rLen = 10240;
                            int cPos = 0;

                            while (rLen > 0)
                            {
                                if (((CancellationToken)cancel).IsCancellationRequested)
                                {
                                    _socket.Close();
                                    _socket.Dispose();

                                    return new MODEL_DATA_STRUCT();
                                }


                                _socket.SendData(data.data, cPos, rLen);

                                Device.BeginInvokeOnMainThread(() => NotifyProgress?.Invoke(this, new ProgressEventArgs(cPos, tLen, ConnectionState.Transmitting)));
                                
                                await Task.Yield();

                                cPos += rLen;
                                if (tLen - cPos < rLen) rLen = tLen - cPos;
                            }

                        }
                        else
                        {
                           _socket.SendData(data.data);

                        }
                    }


                    if (((CancellationToken)cancel).IsCancellationRequested)
                    {
                        _socket.Close();
                        _socket.Dispose();

                        return new MODEL_DATA_STRUCT();
                    }

                    NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Receiving));
                    await Task.Yield();

                    data = MODEL_DATA_STRUCT.ReceiveStruct(_socket);

                    if (((CancellationToken)cancel).IsCancellationRequested)
                    {
                        _socket.Close();
                        return new MODEL_DATA_STRUCT();
                    }
                    
                    switch(data.version) 
                    {
                        case 0:
                        case 1:
                            rslen = 30;
                            break;

                        case 2:
                            rslen = MODEL_DATA_STRUCT.STRUCT_LEN;
                            break;

                        default:
                            rslen = 30;
                            break;
                    }

                    if (data.size > rslen)
                    {
                        int count = data.size - rslen;
                        int seek = 0;
                        int c;
                        int read = 0;

                        ret = new byte[count];

                        while (seek < count)
                        {
                            if (((CancellationToken)cancel).IsCancellationRequested)
                            {
                                _socket.Close();
                                return new MODEL_DATA_STRUCT();
                            }

                            read = (count - seek < 1024) ? count - seek : 1024;

                            c = _socket.ReceiveData(ret, seek, read);

                            seek += c;

                            NotifyProgress?.Invoke(this, new ProgressEventArgs(seek, count, ConnectionState.Receiving));
                            await Task.Yield();

                        }

                        data.data = ret;
                    }

                    _socket.Close();
                    _socket.Dispose();

                    NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Disconnected));
                    await Task.Yield();
                }
                catch
                {
                    _socket?.Close();
                    _socket?.Dispose();

                    NotifyProgress?.Invoke(this, new ProgressEventArgs(0, 100, ConnectionState.Disconnected));
                    await Task.Yield();

                    data.data = null;
                    data.command = 0;
                    data.result = 0;
                    data.size = 5;

                    return data;
                }

            }

            return data;
        }




        #endregion


    }
}
