﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.IO;

namespace CDDMobile.Network
{
    public class BTResult
    {

        public CommandResult Result { get; set; }

        public ImageSource Infrared { get; set; }

        public ImageSource Visible { get; set; }

        public ImageSource Ultraviolet { get; set; }

        public ImageSource Imprint { get; set; }


        public Stream IrStream { get; set;  }
        public Stream VisStream { get; set; }
        public Stream UvStream { get; set; }
        public Stream ImpStream { get; set; }



    }

}
