﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDDMobile.Network
{
    public class ProgressEventArgs : EventArgs
    {

        public int Position { get; private set; }

        public int Total { get; private set; }

        public string Message { get; private set; }

        public ConnectionState State { get; private set; }

        public ProgressEventArgs(int pos, int total)
        {
            Position = pos;
            Total = total;
            State = ConnectionState.Receiving;
        }

        public ProgressEventArgs(ConnectionState state, string message = null)
        {
            State = state;
            Message = message;
            Position = Total = 0;
        }

        public ProgressEventArgs(int pos, int total, ConnectionState state, string message) : this(pos, total)
        {
            State = state;
            Message = message;
        }


        public ProgressEventArgs(int pos, int total, ConnectionState state) : this(pos, total)
        {
            State = state;
        }
        
        public ProgressEventArgs(int pos, int total, string message) : this(pos, total)
        {
            Message = message;
        }

        public ProgressEventArgs(ConnectionState state)
        {
            State = state;
        }

    }

}
