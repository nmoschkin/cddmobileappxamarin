﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CDDMobile;
using CDDMobile.Droid;
using Xamarin.Forms;
using Android.Bluetooth;
using Java.Util;
using System.Threading.Tasks;
using CDDMobile.Network;
using System.IO;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: Dependency(typeof(CDDMobile.Droid.Bluetooth.BTSerial))]
namespace CDDMobile.Droid.Bluetooth
{
    public class BTSerial : IBTSerial
    {
        private static Activity main;

        public static void Init(Activity mainActivity)
        {
            main = mainActivity;
        }

        public string DeviceName { get; set; }

        public bool HasAdapter
        {
            get
            {
                BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
                return (adapter != null);
            }
        }

        public bool AdapterEnabled
        {
            get
            {
                BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
                return (adapter.IsEnabled);
            }
        }

        public bool EnableAdapter()
        {
            BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
            return adapter.Enable();
        }

        public async Task<ISocketStream> GetSocket()
        {

            BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
            
            if (adapter == null) return null;
            // throw new Exception("No Bluetooth adapter found.");

            if (!adapter.IsEnabled)
            {
                return null;
            }

            // throw new Exception("Bluetooth adapter is not enabled.");

            BluetoothDevice device = (from bd in adapter.BondedDevices
                                      where bd.Name == DeviceName
                                      select bd).FirstOrDefault();

            var _socket = device.CreateRfcommSocketToServiceRecord(UUID.FromString("00001101-0000-1000-8000-00805f9b34fb"));
            
            try
            {
                await _socket.ConnectAsync();
            }
            catch
            {
                return null;
            }

            if (_socket.IsConnected)
            {
                return new BTSocket(_socket);
            }
            else
            {
                return null;
            }

        }

        public IList<BTDevice> GetDeviceList(int btclass = 0x180620)
        {
            var res = new List<BTDevice>();
            BTDevice btdev;

            BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
            if (adapter == null) return null;
            // throw new Exception("No Bluetooth adapter found.");

            if (!adapter.IsEnabled) return null;
                // throw new Exception("Bluetooth adapter is not enabled.");

            var devs = adapter.BondedDevices;

            foreach (var dev in devs)
            {
                int dc = int.Parse(dev.BluetoothClass.ToString(), System.Globalization.NumberStyles.HexNumber);

                if (btclass != 0)
                {
                    if (btclass != 0 && dc != btclass) continue;
                }

                btdev = new BTDevice(dev.Address, dev.Name, dc);
                res.Add(btdev);
            }
            
            return res;
        }

        public void OpenBluetoothSettings()
        {
            Intent intentOpenBluetoothSettings = new Intent();
            intentOpenBluetoothSettings.SetAction(Android.Provider.Settings.ActionBluetoothSettings);
            main.StartActivity(intentOpenBluetoothSettings);
        }
    }
}