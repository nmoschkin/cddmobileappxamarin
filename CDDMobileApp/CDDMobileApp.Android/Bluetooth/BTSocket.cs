﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CDDMobile;
using CDDMobile.Droid;
using Xamarin.Forms;
using Android.Bluetooth;
using Java.Util;
using System.Threading.Tasks;
using CDDMobile.Network;
using System.IO;

namespace CDDMobile.Droid.Bluetooth
{
    public class BTSocket : ISocketStream
    {
        private BluetoothSocket socket;

        internal BTSocket(BluetoothSocket source)
        {
            socket = source;
        }

        public void Dispose() => socket.Dispose();

        public void Close()
        {
            socket?.Close();
        }

        public int ReceiveData(byte[] buffer, int offset, int count)
        {
            return socket.InputStream.Read(buffer, offset, count);
        }

        public void SendData(byte[] buffer)
        {
            socket.OutputStream.Write(buffer);
        }

        public void SendData(byte[] buffer, int offset, int count)
        {
            socket.OutputStream.Write(buffer, offset, count);
        }

    }
}