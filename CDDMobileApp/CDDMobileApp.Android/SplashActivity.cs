﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using System;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace CDDMobile.Droid
{
    [Activity(Label = "CDD Controller", Icon = "@mipmap/icon", Theme = "@style/SplashScreen", MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashActivity : Activity
    {

        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {

            base.OnCreate(savedInstanceState, persistentState);

        }

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();

            //System.Threading.Tasks.Task startupWork = new System.Threading.Tasks.Task(() => { StartUp(); });
            //startupWork.Start();

            StartUp();
        }

        void StartUp()
        {
            var intent = new Intent(Application.Context, typeof(MainActivity));

            if (Intent.Extras != null)
                intent.PutExtras(Intent.Extras); // copy push info from splash to main

            StartActivity(intent);
        }

    }
}