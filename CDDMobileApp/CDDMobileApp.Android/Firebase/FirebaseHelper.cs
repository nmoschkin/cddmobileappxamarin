﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Database;
using CDDMobile.Interfaces;
using Xamarin.Forms;
using Java.Interop;
using DataTools.Text;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json.Linq;

[assembly: Dependency(typeof(CDDMobile.Droid.FirebaseHelper))]
namespace CDDMobile.Droid
{
    public class FirebaseHelper : Java.Lang.Throwable, IFirebaseHelper
    {

        FirebaseApp app;
        DatabaseReference mDatabase;

        public FirebaseHelper()
        {
            app = FirebaseApp.Instance;
            mDatabase = FirebaseDatabase.Instance.Reference;
        }

        public async Task<object> QueryAsync(string databasePath, string match)
        {
            var tree = TextTools.Split(databasePath, "\\");

            var child = mDatabase.Child(tree[0]);

            int i, c = tree.Length;

            for (i = 1; i < c; i++)
            {
                child = child.Child(tree[i]);
            }

            var listener = new ValueEventListener(child);

            child.AddListenerForSingleValueEvent(listener);

            do
            {
                await Task.Yield();

            } while (listener.Completed == false);

            return listener.Data;

        }

        public async Task<object> QueryAsync(string databasePath)
        {
            return await QueryAsync(databasePath, null);
        }

        public async Task UpdateAsync(string databasePath, object value)
        {
            var tree = TextTools.Split(databasePath, "\\");

            var child = mDatabase.Child(tree[0]);

            int i, c = tree.Length;

            for (i = 1; i < c; i++)
            {
                child = child.Child(tree[i]);
            }



        }

    }

    internal class ValueEventListener : Java.Lang.Object, IValueEventListener
    {

        public DatabaseReference Reference { get; private set; }

        public bool Completed { get; private set; } = false;

        public string Data { get; private set; } = null;

        public bool Error { get; private set; } = false;


        public ValueEventListener(DatabaseReference reference)
        {
            Reference = reference;
        }

        public void OnCancelled(DatabaseError error)
        {
            
            Device.BeginInvokeOnMainThread(() =>
            {
                Completed = true;
                Error = true;
                Data = error.Message;

            });
        }

        public void OnDataChange(DataSnapshot snapshot)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(snapshot.Value);

            Device.BeginInvokeOnMainThread(() =>
            {
                Completed = true;
                Error = false;

                //if (!snapshot.HasChildren)
                //{

                Data = json;
                return;

                //}

                //var children = snapshot.Children.ToEnumerable();

                //var pOut = new StringBuilder();
                //int x = 0;

                //pOut.Append("[");
                //foreach (DataSnapshot child in children)
                //{
                //    if (x++ > 0) pOut.Append(",");
                //    pOut.Append(Newtonsoft.Json.JsonConvert.SerializeObject(child.Value));
                //}

                //pOut.Append("]");
                //Data = pOut.ToString();


            });

        }
    }


}


