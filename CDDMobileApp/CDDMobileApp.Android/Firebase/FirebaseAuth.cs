﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Xamarin.Forms;
using CDDMobile.Interfaces;
using System.Threading.Tasks;

[assembly: Dependency(typeof(CDDMobile.Droid.FirebaseAuth))]
namespace CDDMobile.Droid
{
    public class FirebaseAuth : IAuth
    {
        public async Task<string> LoginWithEmailPassword(string email, string password)
        {
            var auth = Firebase.Auth.FirebaseAuth.Instance;

            try
            {
                var user = await auth.SignInWithEmailAndPasswordAsync(email, password);
                var sToken = await user.User.GetIdTokenAsync(true);

                return sToken.Token;
                
            }
            catch (FirebaseAuthInvalidUserException e)
            {
                e.PrintStackTrace();
                return "";
            }
        }
    }
}