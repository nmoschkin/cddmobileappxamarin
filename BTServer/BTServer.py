
# (C) Precise Soft

import sys
import socket
import os
import io
import cfdd
import subprocess
import netifaces
import json
import struct

os.system('sdptool add --channel=22 SP')

# Imagining and Object Transfer, Primary Class: Camera
os.system('hciconfig hci0 class 180620')

# Auto-detect host MAC address
hciout = subprocess.run(['hciconfig'], stdout=subprocess.PIPE).stdout.decode('utf-8')

idx = hciout.find("BD Address: ")
hostMACAddress = ''

if (idx > -1):
    idx += 12
    hostMACAddress = hciout[idx:idx+17]
else:
    quit()

print("Host MAC Address is %s" % hostMACAddress)

port = 22 
size = 1024

bufflen = 10240

v1structSize = 30
v2structSize = 82

modelName = "model.tflite"

rootdir = "/cfdd_models"

try:
    
    while True:

        server_sock = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
        server_sock.bind((hostMACAddress,port))

        server_sock.listen(1)

        port = server_sock.getsockname()[1]

        # uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
        print("Waiting for connection on RFCOMM channel %d" % port)

        client_sock, client_info = server_sock.accept()
        print("Accepted connection from ", client_info)

        try:
            while True:
                
                structSize = v1structSize

                data = client_sock.recv(4) # 4 bytes
                if len(data) == 0: break
                
                _len = int.from_bytes(data, byteorder='little')   

                data = client_sock.recv(1)  # 1 byte
                _cmd = data[0]

                
                # negotiation
                if (_cmd == 0x81):

                    _inetport = 3270
                    
                    _nlenOut = 8
                    _ndataOut = []                 

                    _nboolOut = 0

                    _naddrs = netifaces.ifaddresses('wlan0')

                    if (len(_naddrs) > 0 and len(_naddrs[netifaces.AF_INET]) > 0):
                        _naddr = _naddrs[netifaces.AF_INET][0]

                        if (len(_naddr) > 0):
                            print ("Picking WLAN0 IP Address: " + _naddr['addr'])

                            _ndataOut = _naddr['addr'].encode()
                            _nlenOut += len(_ndataOut)
                            _nboolOut = 1  

                    _nblenOut = _nlenOut.to_bytes(4, byteorder='little')
                    client_sock.send(_nblenOut)

                    _nbbOut = _nboolOut.to_bytes(4, byteorder='little')
                    client_sock.send(_nbbOut)
                    
                    if (_nboolOut != 0):

                        client_sock.send(_ndataOut)

                        data = client_sock.recv(4) # 4 bytes
                        if len(data) == 0: break
                
                        _res = int.from_bytes(data, byteorder='little')   
                    
                        if (_res != 0):

                            server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                            server_sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEPORT,1)
                            server_sock.settimeout(30)

                            server_sock.bind((_naddr['addr'], _inetport))                  
                            server_sock.listen(1)

                            _inetport = server_sock.getsockname()[1]

                            print("Waiting for connection on INET interface WLAN0 port %d" % _inetport)

                            _inet_client_sock, _inet_client_info = server_sock.accept()

                            print("Accepted connection from ", _inet_client_info)

                            client_sock.close()
                            client_sock = _inet_client_sock

                    data = client_sock.recv(4) # 4 bytes
                    if len(data) == 0: break
                
                    _len = int.from_bytes(data, byteorder='little')   

                    data = client_sock.recv(1)  # 1 byte
                    _cmd = data[0]

                data = client_sock.recv(v1structSize - 13)

                vbytes = client_sock.recv(4)
                version = int.from_bytes(vbytes, byteorder='little')

                res1 = client_sock.recv(4)

                if (version >= 2): 
                    print("Received Version 2 Struct")
                    data = client_sock.recv(v2structSize - v1structSize)
                    structSize = v2structSize
                else:
                    print("Received Version 1 Struct")
                    structSize = v1structSize
                
                if (_len - structSize > bufflen):
                    
                    _tlen = _len - structSize
                    _llen = _tlen

                    _rlen = bufflen
                    _clen = 0

                    data  = bytearray()

                    
                    while (_rlen > 0):

                        chunk = client_sock.recv(_rlen, 256)
                        _clen = len(bytearray(chunk))

                        #print("tlen: {0} llen: {1} rlen: {2}, clen: {3}".format(_tlen, _llen, _rlen, _clen))
                                                
                        if (_clen == 0): break
                        
                        data.extend(chunk)
                        
                        _llen = _llen - _clen

                        if (_llen < _rlen): _rlen = _llen

                else:
                    data = client_sock.recv(_len - structSize, 256)

                if (_cmd & 0x80) == 0x80:
                    bytes = bytearray(data)
                   
                    medbyte, split, part2 = bytes.partition(bytearray(b'\x00'))
                    typebyte, split, data = part2.partition(bytearray(b'\x00'))

                    result = 1

                    if (bytearray(medbyte)).count == 0 or (bytearray(typebyte).count == 0): 
                        print("Error receiving data.")
                        result = 0

                    else:
                        med = "".join( chr(x) for x in bytearray(medbyte))
                        type = "".join( chr(x) for x in bytearray(typebyte))

                        print("Received DEPLOY command for drugId: {0}; lightingType: {1}".format(med, type))

                        cfdd.add_update_model(med, type, data)

                    retsize = v1structSize

                    vis_size = 0
                    uv_size = 0
                    ir_size = 0
                    imp_size = 0

                    retsize += (uv_size + vis_size + ir_size + imp_size)

                    print("Sending back %d bytes" % retsize)

                    byteout = retsize.to_bytes(4, byteorder='little')

                    client_sock.send(byteout)

                    client_sock.send(_cmd.to_bytes(1, byteorder='little'))

                    if (result & 1):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    client_sock.send(vis_size.to_bytes(4, byteorder='little'))
                    client_sock.send(uv_size.to_bytes(4, byteorder='little'))
                    client_sock.send(ir_size.to_bytes(4, byteorder='little'))
                    client_sock.send(imp_size.to_bytes(4, byteorder='little'))

                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))

                elif (_cmd & 1) == 0:

                    med = "".join( chr(x) for x in bytearray(data))
                    print("Received IDENTIFY command for Drug: %s" % med)

                    # EXECUTE IDENTIFY COMMAND HERE
                    result, values, uv_img, vis_img, ir_img, imp_img = cfdd.detect(med)
                    
                    # REFERENCE CAPTURED IMAGE HERE

                    retsize = v2structSize

                    if (_cmd & 2):
                        vis_size = len(vis_img)
                    else:
                        vis_size = 0
                       
                    if (_cmd & 4):
                        uv_size = len(uv_img)
                    else:
                        uv_size = 0
                       
                    if (_cmd & 8):
                        ir_size = len(ir_img)
                    else:
                        ir_size = 0

                    if (_cmd & 16):
                        imp_size = len(imp_img)
                    else:
                        imp_size = 0


                    retsize += (uv_size + vis_size + ir_size + imp_size)

                    print("Sending back %d bytes" % retsize)

                    byteout = retsize.to_bytes(4, byteorder='little')

                    client_sock.send(byteout)

                    client_sock.send(_cmd.to_bytes(1, byteorder='little'))

                    if (result == 0):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    client_sock.send(vis_size.to_bytes(4, byteorder='little'))
                    client_sock.send(uv_size.to_bytes(4, byteorder='little'))
                    client_sock.send(ir_size.to_bytes(4, byteorder='little'))
                    client_sock.send(imp_size.to_bytes(4, byteorder='little'))

                    # we're sending back a version 2 structure
                    version = 2

                    client_sock.send(version.to_bytes(4, byteorder='little'))
                    
                    #reserved2
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))

                    if (values[1][0] == 0):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    dval = bytearray(struct.pack('d', float(values[1][1])))
                    print("Vis dVal Length: ", len(dval))

                    client_sock.send(dval)

                    if (values[0][0] == 0):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    dval = bytearray(struct.pack('d', float(values[0][1])))
                    print("UV dVal Length: ", len(dval))

                    client_sock.send(dval)

                    if (values[2][0] == 0):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    dval = bytearray(struct.pack('d', float(values[2][1])))
                    print("IR dVal Length: ", len(dval))

                    client_sock.send(dval)

                    if (values[3][0] == 0):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    dval = bytearray(struct.pack('d', float(values[3][1])))
                    print("Imp dVal Length: ", len(dval))

                    client_sock.send(dval)

                    print("Raw Structure: ", values)

                                        
                    #reserved3
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))

                    #reserved4
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))

                    #reserved5
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))

                    #reserved6
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))



                    if (_cmd & 2):
                        imgdata = vis_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent
                                                    
                        stream.close()

                    if (_cmd & 4):
                        imgdata = uv_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent

                                                    
                        stream.close()

                    if (_cmd & 8):
                        imgdata = ir_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent

                            
                        stream.close()

                    if (_cmd & 16):
                        imgdata = imp_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent

                            
                        stream.close()

                elif (_cmd & 1) == 1:
                    print("Received CAPTURE command")
                    
                    # EXECUTE CAPTURE COMMAND HERE
                    uv_img, vis_img, ir_img, imp_img = cfdd.detect_without_prediction()
                    
                    # this function always succeeds
                    result = 1
                    # REFERENCE CAPTURED IMAGE HERE

                    retsize = v1structSize

                    if (_cmd & 2):
                        vis_size = len(vis_img)
                    else:
                        vis_size = 0
                       
                    if (_cmd & 4):
                        uv_size = len(uv_img)
                    else:
                        uv_size = 0
                       
                    if (_cmd & 8):
                        ir_size = len(ir_img)
                    else:
                        ir_size = 0

                    if (_cmd & 16):
                        imp_size = len(imp_img)
                    else:
                        imp_size = 0


                    retsize += (uv_size + vis_size + ir_size + imp_size)

                    print("Sending back %d bytes" % retsize)

                    byteout = retsize.to_bytes(4, byteorder='little')

                    client_sock.send(byteout)

                    client_sock.send(_cmd.to_bytes(1, byteorder='little'))

                    if (result & 1):
                        client_sock.send(bytearray(b'\x01'))
                    else:
                        client_sock.send(bytearray(b'\x00'))

                    client_sock.send(vis_size.to_bytes(4, byteorder='little'))
                    client_sock.send(uv_size.to_bytes(4, byteorder='little'))
                    client_sock.send(ir_size.to_bytes(4, byteorder='little'))
                    client_sock.send(imp_size.to_bytes(4, byteorder='little'))

                    # client_sock.send(bytearray(b'\x00\x00\x00\x00'))
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))
                    client_sock.send(bytearray(b'\x00\x00\x00\x00'))

                    if (_cmd & 2):
                        imgdata = vis_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent
                                                    
                        stream.close()

                    if (_cmd & 4):
                        imgdata = uv_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent

                                                    
                        stream.close()

                    if (_cmd & 8):
                        imgdata = ir_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent

                            
                        stream.close()

                    if (_cmd & 16):
                        imgdata = imp_img

                        stream = io.BytesIO(imgdata)
                        stream.seek(0)
                        lseek = 0

                        while True:
                            stream.seek(lseek)
                            chunk = stream.read(1024)

                            if (len(chunk) == 0): 
                                break

                            sent = client_sock.send(chunk)
                            lseek += sent

                            
                        stream.close()

                break

        except IOError as e:
            print(e)
            pass

        print("disconnected")

        client_sock.shutdown(socket.SHUT_RDWR)
        client_sock.close()

        server_sock.shutdown(socket.SHUT_RDWR)
        server_sock.close()
        print("all done")

except IOError:
    pass
