import sys
from cfdd import main

def cfdd_command():
    args = sys.argv[1:]
    if len(args) < 1:
        print('usage: ...')
    elif(args[0]=='capture'):
        print("taking photos...")
        main.detect_without_prediction()
        print("done.")
    elif(args[0]=='detect'):
        print("start detection...")
        main.detect(args[1])
        print("done.")

