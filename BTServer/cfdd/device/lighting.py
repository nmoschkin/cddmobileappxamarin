from periphery import GPIO

def is_light_type_valid(lighting_type):
    if(lighting_type!='uv' and lighting_type!='vis' and lighting_type!='ir' and lighting_type!='imp'):
        return False
    else:
        return True

class Light:

    def __init__(self, uv_io=6, vis_io=7, ir_io=8): 
        """Initialize gpio ports.
        Args:
            uv_io (int, optional): Port # of UV light.
            vis_io (int, optional): Port # of VIS light.
            ir_io (int, optional): Port # of IR light.

        """
        # Open GPIOs with output direction
        self.gpio_uv = GPIO(uv_io, "out") # UV Light
        self.gpio_vis = GPIO(vis_io, "out") # white Light
        self.gpio_ir = GPIO(ir_io, "out") # IR Light
        self.ON = False
        self.OFF = True

    def turn_on(self, light):
        """Turn on one type of light.
        Args:
            light (str): Can be one of the light 'uv', 'vis' or 'ir'.

        """
        if(light=="uv"):
            self.gpio_uv.write(self.ON)
            self.gpio_vis.write(self.OFF)
            self.gpio_ir.write(self.OFF)
        elif(light=="vis"):
            self.gpio_uv.write(self.OFF)
            self.gpio_vis.write(self.ON)
            self.gpio_ir.write(self.OFF)
        elif(light=="ir"):
            self.gpio_uv.write(self.OFF)
            self.gpio_vis.write(self.OFF)
            self.gpio_ir.write(self.ON)
        else:
            turn_off()

    def turn_off(self):
        """Turn off all the lights.

        """
        self.gpio_uv.write(self.OFF)
        self.gpio_vis.write(self.OFF)
        self.gpio_ir.write(self.OFF)

    def close(self):
        """Close all gpio ports

        """
        self.gpio_uv.close()
        self.gpio_vis.close()
        self.gpio_ir.close()
    
