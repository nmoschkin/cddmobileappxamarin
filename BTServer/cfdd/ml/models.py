
import os
from cfdd.device import lighting

MODEL_DIR = '/cfdd_models'

def get_model_path(drug_id, lighting_type):
    """Get model file path.
    Args:
        drug_id (:obj:`str`): Unique drug id.
        lighting_type (:obj:`str`): Can be one of 'uv', 'vis', 'ir' or 'imp'.
    Returns:
        str: Absolute path of the model

    """
    if(not lighting.is_light_type_valid(lighting_type)):
        raise ValueError("Input lighting type is not valid.")
        
    return '{}/{}/{}.tflite'.format(MODEL_DIR, drug_id, lighting_type)


def is_model_exist(drug_id, lighting_type):
    """Check if model file exist in the system.
    Args:
        drug_id (:obj:`str`): Unique drug id.
        lighting_type (:obj:`str`): Can be one of 'uv', 'vis', 'ir' or 'imp'.
    Returns:
        boolean: True/False
        
    """
    if os.path.exists(get_model_path(drug_id, lighting_type)):
        return True
    else:
        return False


def add_update_model(drug_id, lighting_type, model_stream):
    """Add/Update pre-trained model into MODEL_DIR.
    Args:
        drug_id (:obj:`str`): Unique drug id.
        lighting_type (:obj:`str`): Can be one of 'uv', 'vis', 'ir' or 'imp'.
        model_stream (:obj): The input stream of the model
    Raises:
        ValueError: If lighting_type is not equals to 'uv', 'vis', 'ir' or 'imp'.

    """
    if(not lighting.is_light_type_valid(lighting_type)):
        raise ValueError("Input lighting type is not valid.")

    model_path = get_model_path(drug_id, lighting_type)
    # Create directory if not exist.
    os.makedirs(os.path.dirname(model_path), exist_ok=True)

    with open(model_path, 'wb') as file:
        file.write(model_stream)


def remove_model(drug_id):
    """Remove all pre-trained models related to this drug_id
    Args:
        drug_id (:obj:`str`): Unique drug id.
        
    """
    uv_model = get_model_path(drug_id, 'uv')
    if os.path.exists(uv_model):
        os.remove(uv_model)
    
    vis_model = get_model_path(drug_id, 'vis')
    if os.path.exists(vis_model):
        os.remove(vis_model)

    ir_model = get_model_path(drug_id, 'ir')
    if os.path.exists(ir_model):
        os.remove(ir_model)

    imp_model = get_model_path(drug_id, 'imp')
    if os.path.exists(imp_model):
        os.remove(imp_model)



