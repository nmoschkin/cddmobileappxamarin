from PIL import Image
import numpy as np
import cv2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import time

def image_auto_crop(original_img):

    # Crop Image Automatically
    # Parameter
    CroppedImgHeight = 500
    CroppedImgWidth = 500
    CropStartPointX = 470
    CropStartPointY = 80

    imgArray = np.array(original_img)

    CroppedArray = imgArray[CropStartPointY: CropStartPointY+CroppedImgHeight, CropStartPointX: CropStartPointX+CroppedImgWidth, : ]
    cropped_img = Image.fromarray(CroppedArray, 'RGB')

    return cropped_img

def imprint_generation(ir_img):

    # Crop Image Automatically
    # Parameter
    CroppedImgHeight = 500
    CroppedImgWidth = 500
    CropStartPointX = 470
    CropStartPointY = 80

    imgArray = np.array(ir_img)

    CroppedArray = imgArray[CropStartPointY: CropStartPointY+CroppedImgHeight, CropStartPointX: CropStartPointX+CroppedImgWidth, : ]
    cropped_img = Image.fromarray(CroppedArray, 'RGB').convert('L')
    # print(type(cropped_img))
    cropped_array_img = np.array(cropped_img)
    # print(cropped_array_img.shape)
    # Laplacian Gradient Processing
    laplacian = cv2.Laplacian(cropped_array_img, cv2.CV_64F, ksize=9)

    # Transfer Laplacian matrix to the best visual format
    # Using matplotlib is the best solution up to now
    plt.imshow(laplacian, cmap='gray')
    plt.xticks([]), plt.yticks([])

    # Save the image
    SaveFolder = '/tmp/cfdd_outputs/'
    ImgName = "cfdd_capture_imp_laplacian_{}.jpg".format(time.strftime("%Y%m%d-%H%M%S"))
    plt.savefig(SaveFolder+ImgName, bbox_inches='tight',pad_inches=0.0)
    #imp_fig = plt.gcf()
    #print(type(imp_img))
    plt.close()

    imp_img_laplacian = cv2.imread(SaveFolder+ImgName, 0)
    
    # CLAHE processing
    clahe = cv2.createCLAHE(clipLimit = 2.0, tileGridSize=(8,8))
    # CLAHE
    clahe_img = clahe.apply(imp_img_laplacian)
    
    # Histogram Equalization
    # clahe_img = cv2.equalizeHist(imp_img_laplacian)
    ImpImgName = "cfdd_capture_imp_{}.jpg".format(time.strftime("%Y%m%d-%H%M%S"))

    plt.imshow(clahe_img, cmap='gray')
    plt.xticks([]), plt.yticks([])



    plt.savefig(SaveFolder+ImpImgName, bbox_inches='tight', pad_inches=0.0)

    imp_img = Image.open(SaveFolder+ImpImgName)

    x, y = (imp_img.size)

    imp_img = imp_img.crop((15, 15, x-15, y-15))
    
    return imp_img
