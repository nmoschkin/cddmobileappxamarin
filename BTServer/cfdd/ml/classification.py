from edgetpu.classification.engine import ClassificationEngine
import os
from cfdd.ml import models as m
from PIL import Image
import pygame
import re
import numpy as np
import cv2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import time


def ReadLabelFile(file_path):
  """Reads labels from text file and store it in a dict.

  Each line in the file contains id and description separted by colon or space.
  Example: '0:cat' or '0 cat'.

  Args:
    file_path: String, path to the label file.

  Returns:
    Dict of (int, string) which maps label id to description.
  """
  with open(file_path, 'r', encoding='utf-8') as f:
    lines = f.readlines()
    ret = {}
    for line in lines:
        pair = re.split(r'[:\s]+', line.strip(), maxsplit=1)
        ret[int(pair[0])] = pair[1].strip()
    return ret

def ImageMarginCrop(image, margin_length): # The image should be PIL image
    ImageArray = np.asarray(image)
    [width, height, dimension] = ImageArray.shape
    CroppedImageArray = ImageArray[margin_length : width - margin_length, margin_length : height - margin_length, :]
    CroppedImage = Image.fromarray(CroppedImageArray)
    return CroppedImage

def ParseResult(result):

    highRes = 0
    high = float(0)
    output = [0, float(0)]

    for r in result:
        if (float(r[1]) >= high):
            highRes = r[0]
            high = float(r[1])

    output[0] = highRes
    output[1] = high

    return output


def predict(drug_id, uv_image, vis_image, ir_image, imp_image):
    """Make predictions using models.
    Args:
        drug_id (:obj:`str`): Unique drug id.
        uv_image (bytes): Image under UV light.
        vis_image (bytes): Image under VIS light.
        ir_image (bytes): Image under IR light.cf
        imp_image (bytes): Image After Laplacian gradient processing of ir_image.
    Returns:
        list: List of results

    """
    if(drug_id==''):
        raise ValueError('Input drug_id cannot be empty.')

    if(m.is_model_exist(drug_id, 'uv') and m.is_model_exist(drug_id, 'vis') and m.is_model_exist(drug_id, 'ir') and m.is_model_exist(drug_id, 'imp')):
        uv_model_path = m.get_model_path(drug_id, 'uv')
        vis_model_path = m.get_model_path(drug_id, 'vis')
        ir_model_path = m.get_model_path(drug_id, 'ir')
        imp_model_path = m.get_model_path(drug_id, 'imp')
    else:
        raise IOError('Less than 4 models are found for drug: {}'.format(drug_id))

    # Prepare labels.
    labels = {0:'authentic', 1:'counterfeit'}
    #labels = ReadLabelFile("/home/mendel/dev/models/vis_dict.txt")

    results = [[0,float(0)],[0,float(0)],[0,float(0)],[0,float(0)]]

    # Predict uv image.
    engine = ClassificationEngine(uv_model_path)
    #pil_string_uv_image = pygame.image.tostring(uv_image,"RGB",False)
    #uv_image = Image.frombytes("RGB",(1280,720), pil_string_uv_image)
    result = engine.classify_with_image(uv_image, top_k=3)

    # Temporary code
    flipped_result = [] 
    for r in result:
        rList = list(r)
        if (r[0] == 0):
            rList[0] = 1
        else:
            rList[0] = 0
        flipped_result.append(tuple(rList))
    result = flipped_result[:]
    # End of Temporary code

    results[0] = ParseResult(result)

    print("UV detection finished.")

    for r in result:
        print(labels[r[0]])
        print('Score : ', r[1])
        print('---------------------------')

    # Predict vis image.
    engine = ClassificationEngine(vis_model_path)
    #pil_string_vis_image = pygame.image.tostring(vis_image,"RGB",False)
    #vis_image = Image.frombytes("RGB",(1280,720), pil_string_vis_image)
    #vis_image = Image.open("/tmp/cfdd_outputs/cfdd_capture__20190621-192121.jpg")
    result = engine.classify_with_image(vis_image, top_k=3)
     
    # Temporary code
    #flipped_result = [] 
    #for r in result:
    #    rList = list(r)
    #    if (r[0] == 0):
    #        rList[0] = 1
    #    else:
    #        rList[0] = 0
    #    flipped_result.append(tuple(rList))
    #result = flipped_result[:]
    # End of Temporary code
    
    results[1] = ParseResult(result)

    print("VIS detection finished.")
    for r in result:
        print(labels[r[0]])
        print('Score : ', r[1])
        print('---------------------------')

    # Predict ir image.
    #engine = ClassificationEngine(ir_model_path)
    ##pil_string_ir_image = pygame.image.tostring(ir_image,"RGB",False)
    ##ir_image = Image.frombytes("RGB",(1280,720), pil_string_ir_image)
    #result = engine.classify_with_image(ir_image, top_k=3)

    ## Temporary code
    #flipped_result = [] 
    #for r in result:
    #    rList = list(r)
    #    if (r[0] == 0):
    #        rList[0] = 1
    #    else:
    #        rList[0] = 0
    #    flipped_result.append(tuple(rList))
    #result = flipped_result[:]
    ## End of Temporary code

    #results['ir'] = ParseResult(result)

    results[2][0] = 0
    results[2][1] = float(0.0)

    #print("IR detection finished.")

    #for r in result:
    #    print(labels[r[0]])
    #    print('Score : ', r[1])
    #    print('---------------------------')

    # Predict imp image.
    
    engine = ClassificationEngine(imp_model_path)
    # Debug code see if training image works
    # imp_image_training = Image.open("/home/mendel/dev/impImageComp/training_img.jpeg")
    #  result = engine.classify_with_image(imp_image_training, top_k=3)

    # result = engine.classify_with_image(imp_image, top_k=3)
    # End of Debug code
    cropped_imp_image = ImageMarginCrop(imp_image, 15) # Crop Margin is 15 pixels
    cropped_imp_image.save("/tmp/cfdd_outputs/debug.jpg")
    #result = engine.classify_with_image(imp_image, top_k=3)
    result = engine.classify_with_image(cropped_imp_image, top_k=3)

    results[3] = ParseResult(result)

    print("Imp detection finished.")

    for r in result:
        print(labels[r[0]])
        print('Score : ', r[1])
        print('---------------------------')

    return results


def predict_cloud(drug_id, image):
    raise NotImplementedError("")


def analyze_result(result):
    for label, score in result:
        if(label==1):
            if(score>0.5):
                print("Final result: counterfeit")
                return 1
    print("Final result: authentic")
    return 0


    
