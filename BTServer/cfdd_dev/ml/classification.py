from edgetpu.classification.engine import ClassificationEngine
from edgetpu.classification.engine_occ import ClassificationEngineOCC
import os
from cfdd_dev.ml import models as m
from PIL import Image
import pygame
import re
import numpy as np
import cv2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import time


def ReadLabelFile(file_path):
  """Reads labels from text file and store it in a dict.

  Each line in the file contains id and description separted by colon or space.
  Example: '0:cat' or '0 cat'.

  Args:
    file_path: String, path to the label file.

  Returns:it=1)
        ret[int(pair[0])] = pair[1].strip()
    Dict of (int, string) which maps label id to description.
  """
  with open(file_path, 'r', encoding='utf-8') as f:
    lines = f.readlines()
    ret = {}
    for line in lines:
        pair = re.split(r'[:\s]+', line.strip(), maxspl)
    return ret

def predict(drug_id, uv_image, vis_image, ir_image, imp_image):
    """Make predictions using models.
    Args:
        drug_id (:obj:`str`): Unique drug id.
        uv_image (bytes): Image under UV light.
        vis_image (bytes): Image under VIS light.
        ir_image (bytes): Image under IR light.cf
        imp_image (bytes): Image After Laplacian gradient processing of ir_image.
    Returns:
        list: List of results

    """
    if(drug_id==''):
        raise ValueError('Input drug_id cannot be empty.')

    if(m.is_model_exist(drug_id, 'uv') and m.is_model_exist(drug_id, 'vis') and m.is_model_exist(drug_id, 'ir') and m.is_model_exist(drug_id, 'imp')):
        uv_model_path = m.get_model_path(drug_id, 'uv')
        vis_model_path = m.get_model_path(drug_id, 'vis')
        ir_model_path = m.get_model_path(drug_id, 'ir')
        imp_model_path = m.get_model_path(drug_id, 'imp')
    else:
        raise IOError('Less than 4 models are found for drug: {}'.format(drug_id))

    # Prepare labels.
    labels = {0:'authentic', 1:'counterfeit'}
    #labels = ReadLabelFile("/home/mendel/dev/models/vis_dict.txt")

    results = []

    # Predict uv image.
    engine = ClassificationEngine(uv_model_path)
    #pil_string_uv_image = pygame.image.tostring(uv_image,"RGB",False)
    #uv_image = Image.frombytes("RGB",(1280,720), pil_string_uv_image)
    result = engine.classify_with_image(uv_image, top_k=3)
    results += result
    print("UV detection finished.")

    for r in result:
        print('---------------------------')
        print(labels[r[0]])
        print('Score : ', r[1])

    # Predict vis image.
    engine = ClassificationEngine(vis_model_path)
    #pil_string_vis_image = pygame.image.tostring(vis_image,"RGB",False)
    #vis_image = Image.frombytes("RGB",(1280,720), pil_string_vis_image)
    #vis_image = Image.open("/tmp/cfdd_outputs/cfdd_capture__20190621-192121.jpg")
    result = engine.classify_with_image(vis_image, top_k=3)

    # Temporary Code
    flipped_result =[]
    for r in result:
        rList = list(r)
        if(r[0] == 0):
            rList[0]=1
        else:
            rList[0]=0
        flipped_result.append(tuple(rList))
    result = flipped_result[:]
    # End of temporary code
    results += result
    print("VIS detection finished.")
    for r in result:
        print('---------------------------')
        print(labels[r[0]])
        print('Score : ', r[1])

    # Predict ir image.
    engine = ClassificationEngine(ir_model_path)
    #pil_string_ir_image = pygame.image.tostring(ir_image,"RGB",False)
    #ir_image = Image.frombytes("RGB",(1280,720), pil_string_ir_image)
    result = engine.classify_with_image(ir_image, top_k=3)
    results += result
    print("IR detection finished.")

    for r in result:
        print('---------------------------')
        print(labels[r[0]])
        print('Score : ', r[1])

    # Predict imp image.
    engine = ClassificationEngine(imp_model_path)
    result = engine.classify_with_image(imp_image, top_k=3)
    results += result
    print("Imp detection finished.")

    for r in result:
        print('---------------------------')
        print(labels[r[0]])
        print('Score : ', r[1])

    return results

def predict_occ_dev(drug_id, uv_image, vis_image, ir_image, imp_image):
    """Make predictions using models.
       This function is under development and it is designed for one class classification
       Args:
           drug_id (:obj:`str`): Unique drug id.
           uv_image (bytes): Image under UV light.
           vis_image (bytes): Image under VIS light.
           ir_image (bytes): Image under IR light.cf
           imp_image (bytes): Image After Laplacian gradient processing of ir_image.
       Returns:
           list: List of results

       """
    if(drug_id==''):
        raise ValueError('Input drug_id cannot be empty.')

    if (m.is_occ_model_exist(drug_id, 'uv') and m.is_occ_model_exist(drug_id, 'vis') and m.is_occ_model_exist(drug_id,'ir') and m.is_occ_model_exist(drug_id, 'imp')):
        uv_model_path = m.get_occ_model_path(drug_id, 'uv')
        vis_model_path = m.get_occ_model_path(drug_id, 'vis')
        ir_model_path = m.get_occ_model_path(drug_id, 'ir')
        imp_model_path = m.get_occ_model_path(drug_id, 'imp')
    else:
        raise IOError('Less than 4 models are found for drug: {}'.format(drug_id))
    SigProb = m.get_reference_model()

    # Prepare labels.
    labels = {0:'authentic', 1:'counterfeit'}
    #labels = ReadLabelFile("/home/mendel/dev/models/vis_dict.txt")

    results = []

    # Predict uv image.
    # Implement Later

    # Predict vis image.
    engine = ClassificationEngineOCC(vis_model_path)
    result = engine.ClassifyWithImage_OCC(vis_image, threshold = -20, referenceMatrix = SigProb, output_tensor_dim = -4096)
    results.append(result)
    print("VIS detection finished.")
    if result == 1:
        print("Counterfeit Drug")
    else:
        print("Authentic Drug")

    # Predict uv image.
    # Implement Later

    # Predict uv image.
    # Implement Later

    return results

def predict_cloud(drug_id, image):
    raise NotImplementedError("")


def analyze_result(result):
    for label, score in result:
        if(label==1):
            if(score>0.5):
                print("Final result: counterfeit")
                return 1
    print("Final result: authentic")
    return 0

def analyze_result_occ(result):
    for i in result:
        if i == 1:
            print("Final result: counterfeit")
            return 1
    print("Final result: authentic")
    return 0
    
