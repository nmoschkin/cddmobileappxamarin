import pygame
import pygame.camera
import time
import os
from PIL import Image

CAPTURE_IMAGE_DIR = "/tmp/cfdd_outputs"

class Camera:

    def __init__(self, width=1280, height=720, save_image=False):
        """Initialize the camera.
        Args:
            width (int, optional): Frame width.
            height (int, optional): Frame height.
            save_image (boolean): Save captured image into local.

        """
        self.save_image = save_image
        # Initalize camera 
        #  pygame.init()
        pygame.camera.init()
        self.cam = pygame.camera.Camera("/dev/video1", (width, height))

    def capture(self, light_type=''):
        """Take a photo.
        Returns:
            bytes: Image bytes

        """
        self.cam.start()

        image = self.cam.get_image()
        if(self.save_image):
            # Create directory if not exist.
            os.makedirs(CAPTURE_IMAGE_DIR, exist_ok=True)
            image_path = "{}/cfdd_capture_{}_{}.jpg".format(CAPTURE_IMAGE_DIR, light_type, time.strftime("%Y%m%d-%H%M%S"))
            pygame.image.save(image, image_path)
            print("Saved to: " + image_path)
            
        self.cam.stop()

        string_image = pygame.image.tostring(image,"RGB",False)
        img = Image.frombytes("RGB",(1280,720), string_image)

        return img
    


