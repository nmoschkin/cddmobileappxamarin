import unittest
from cfdd_dev import main
from cfdd_dev.ml import models

class Test_cfdd(unittest.TestCase):

    def test_add_update_model(self):
        drug_id = 00000
        lighting_type = 'uv'
        model_stream = str.encode('bsdibisdhhjshjkdshdihgijdshfhhsdfgiusdhgishgjgf')
        models.add_update_model(drug_id, lighting_type, model_stream)

        self.assertTrue(models.is_model_exist(drug_id, lighting_type))

    def test_remove_model(self):
        drug_id = 00000
        models.remove_model(drug_id)

        self.assertFalse(models.is_model_exist(drug_id, 'uv'))
        self.assertFalse(models.is_model_exist(drug_id, 'ir'))
        self.assertFalse(models.is_model_exist(drug_id, 'vis'))

    def detect_without_prediction(self):
        main.detect_without_prediction()
        
    def test_detect(self):
        main.detect(1234)
    


if __name__ == '__main__':
    unittest.main()