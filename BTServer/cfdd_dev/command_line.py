import sys
from cfdd_dev import main
import argparse

def arguments():
    """
    Parse arguments into a parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--libname")
    parser.add_argument("command", default='capture', help= "Command Set: capture / detect")
    parser.add_argument("pillname", nargs ='?', default='tums', help="Pill Name")
    parser.add_argument("-o", "--occ", action='store_true', help="One Class Classification Switch")
    parser.add_argument("-c", "--cnn", action='store_true', help ="CNN model (default)")
    return (parser)

def cfdd_dev_command():

    parser = arguments()
    args = parser.parse_args()
    if args.command == 'capture':
        print("taking photos...")
        main.detect_without_prediction()
        print("done.")
    elif(args.command == 'detect'):
        if args.occ:
            print("start occ detection...")
            main.detect_occ(args.pillname)
            print("done.")
        else:
            print("start detection...")
            main.detect(args.pillname)
            print("done.")
    else:
        print("Wrong Command")


