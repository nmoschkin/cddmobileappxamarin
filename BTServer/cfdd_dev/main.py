from cfdd_dev.device import lighting
from cfdd_dev.device import camera
from cfdd_dev.ml import classification as cl
from cfdd_dev.ml import ImageProcessing as imgProcess
from io import BytesIO

def convToJPEG(img):
    #zdata = StringIO()
    zdata = BytesIO()
    img.save(zdata, 'JPEG')
    return zdata.getvalue()

def detect(drug_id):
    """Start the main detection flow.
    Args:
        drug_id (:obj:`str`): Unique drug id.
    Returns:
        list: List of results

    """
    # Initialize light.
    light = lighting.Light(uv_io=6, vis_io=7, ir_io=8)

    # Inititalize camera.
    # By default image width=1280, height=720.
    cam = camera.Camera(save_image=True)

    # Turn on uv light.
    light.turn_on('uv')
    # Capture uv image.
    uv_img = cam.capture()
    # Crop image
    uv_img_crop = imgProcess.image_auto_crop(uv_img)

    # Turn on vis light.
    light.turn_on('vis')
    # Capture uv image.
    vis_img = cam.capture()
    # Crop image
    vis_img_crop = imgProcess.image_auto_crop(vis_img)

    # Turn on ir light.
    light.turn_on('ir')
    # Capture uv image.
    ir_img = cam.capture()
    # Crop image
    ir_img_crop = imgProcess.image_auto_crop(ir_img)

    # Turn off and Close GPIO.
    light.turn_off()
    light.close()

    # Generate imprint image from IR image
    # Crop is assembled in imprint_generation function
    # imp_img is cropped
    imp_img = imgProcess.imprint_generation(ir_img)
    globals()
    result = cl.predict(drug_id, uv_img_crop, vis_img_crop, ir_img_crop, imp_img)

    return cl.analyze_result(result), convToJPEG(uv_img_crop), convToJPEG(vis_img_crop), convToJPEG(ir_img_crop), convToJPEG(imp_img)

def detect_occ(drug_id):
    """Start the main one-class classification flow.
    Args:
        drug_id (:obj:`str`): Unique drug id.
    Returns:
        list: List of results

    """
    # Initialize light.
    light = lighting.Light(uv_io=6, vis_io=7, ir_io=8)

    # Inititalize camera.
    # By default image width=1280, height=720.
    cam = camera.Camera(save_image=True)

    # Turn on uv light.
    light.turn_on('uv')
    # Capture uv image.
    uv_img = cam.capture()
    # Crop image
    uv_img_crop = imgProcess.image_auto_crop(uv_img)

    # Turn on vis light.
    light.turn_on('vis')
    # Capture uv image.
    vis_img = cam.capture()
    # Crop image
    vis_img_crop = imgProcess.image_auto_crop(vis_img)

    # Turn on ir light.
    light.turn_on('ir')
    # Capture uv image.
    ir_img = cam.capture()
    # Crop image
    ir_img_crop = imgProcess.image_auto_crop(ir_img)

    # Turn off and Close GPIO.
    light.turn_off()
    light.close()

    # Generate imprint image from IR image
    # Crop is assembled in imprint_generation function
    # imp_img is cropped
    imp_img = imgProcess.imprint_generation(ir_img)
    globals()
    result = cl.predict_occ_dev(drug_id, uv_img_crop, vis_img_crop, ir_img_crop, imp_img)

    return cl.analyze_result_occ(result), convToJPEG(uv_img_crop), convToJPEG(vis_img_crop), convToJPEG(ir_img_crop), convToJPEG(imp_img)

def detect_without_prediction():
    """Capture and save all images.
    Args:
        drug_id (:obj:`str`): Unique drug id.

    """
    # Initialize light.
    light = lighting.Light(uv_io=6, vis_io=7, ir_io=8)

    # Inititalize camera.
    # By default image width=1280, height=720.
    cam = camera.Camera(save_image=True)

    # Turn on uv light.
    light.turn_on('uv')
    # Capture uv image.
    uv_img = cam.capture(light_type='uv')
    # Crop image
    uv_img_crop = imgProcess.image_auto_crop(uv_img)

    # Turn on vis light.
    light.turn_on('vis')
    # Capture uv image.vim
    vis_img = cam.capture(light_type='vis')
    # Crop image
    vis_img_crop = imgProcess.image_auto_crop(vis_img)

    # Turn on ir light.
    light.turn_on('ir')
    # Capture uv image.
    ir_img = cam.capture(light_type='ir')
    # Crop image
    ir_img_crop = imgProcess.image_auto_crop(ir_img)

    # Turn off and Close GPIO.
    light.turn_off()
    light.close()

    # Generate imprint image from IR image
    # Crop is assembled in imprint_generation function
    # imp_img is cropped
    imp_img = imgProcess.imprint_generation(ir_img)

    return convToJPEG(uv_img_crop), convToJPEG(vis_img_crop), convToJPEG(ir_img_crop), convToJPEG(imp_img)

if __name__ == "__main__":
    pass
