from periphery import GPIO
import subprocess as sp
import time

def button_callback():
    print("Button was pushed!" + str(time.time()))
    sp.call(['sudo', 'hciconfig', 'hci0', 'piscan'])

gpio_button = GPIO(77,"in")
gpio_button.edge = 'falling'
while True:
    if gpio_button.poll()==True:
        button_callback()


